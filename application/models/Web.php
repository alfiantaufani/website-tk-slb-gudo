<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * @package	:	Website Sekolah
 * @author	:	Fika Ridaul Maulayya (https://www.rubypedia.com)
 * @version	:	Beta Version V.0.1
 * @license	:	Protection
 */
class Web extends CI_Model
{

    //fungsi check 
    function check_one($table, $where)
    {
        $this->db->select('*');
        $this->db->from($table);
        $this->db->where($where);
        $query = $this->db->get();
        if ($query->num_rows() == 0) {
            return FALSE;
        } else {
            return $query->result();
        }
    }

    //count berita
    function count_berita()
    {
        $query = $this->db->select('*')
            ->from('tbl_berita')
            ->order_by('created_at','DESC')
            ->get();

        if($query->num_rows() > 0)
        {
            return $query->num_rows();
        }
        else
        {
            return NULL;
        }
    }

    //index berita
    function index_berita($halaman, $batas)
    {
        $query = $this->db->select('*')
            ->from('tbl_berita')
            ->limit($batas, $halaman)
            ->order_by('created_at','DESC')
            ->get();

        if($query->num_rows() > 0)
        {
            return $query;
        }
        else
        {
            return NULL;
        }
    }

    //detail berita
    function detail_berita($slug)
    {
        $query = $this->db->select('a.id_berita, a.judul_berita, a.slug_berita, a.user_id, a.kategori_id, a.content, a.views, a.gambar, a.tags, a.descriptions, a.created_at, b.id_kategori, b.nama_kategori, b.slug, c.id_user, c.nama_user')
            ->from('tbl_berita a')
            ->join('tbl_kategori b', 'a.kategori_id = b.id_kategori')
            ->join('tbl_users c', 'a.user_id = c.id_user')
            ->where('a.slug_berita', $slug)
            ->get();

        if($query->num_rows() > 0)
        {
            return $query->row();
        }else
        {
            return NULL;
        }
    }

    //count agenda
    function count_agenda()
    {
        $query = $this->db->select('*')
            ->from('tbl_agenda')
            ->order_by('id_agenda','DESC')
            ->get();

        if($query->num_rows() > 0)
        {
            return $query->num_rows();
        }
        else
        {
            return NULL;
        }
    }

    //index agenda
    function index_agenda($halaman, $batas)
    {
        $query = $this->db->select('*')
            ->from('tbl_agenda')
            ->limit($batas, $halaman)
            ->order_by('id_agenda','DESC')
            ->get();

        if($query->num_rows() > 0)
        {
            return $query;
        }
        else
        {
            return NULL;
        }
    }

    //detail agenda
    function detail_agenda($slug)
    {
        $query = $this->db->select('a.id_agenda, a.judul_agenda, a.slug_agenda, a.user_id, a.isi_agenda, a.gambar, a.tags, a.descriptions, a.created_at, b.id_user, b.nama_user')
            ->from('tbl_agenda a')
            ->join('tbl_users b', 'a.user_id = b.id_user')
            ->where('a.slug_agenda', $slug)
            ->get();

        if($query->num_rows() > 0)
        {
            return $query->row();
        }else
        {
            return NULL;
        }
    }

    //count pengumuman
    function count_pengumuman()
    {
        $query = $this->db->select('*')
            ->from('tbl_pengumuman')
            ->order_by('id_pengumuman','DESC')
            ->get();

        if($query->num_rows() > 0)
        {
            return $query->num_rows();
        }
        else
        {
            return NULL;
        }
    }

    //index pengumuman
    function index_pengumuman($halaman, $batas)
    {
        $query = $this->db->select('*')
            ->from('tbl_pengumuman')
            ->limit($batas, $halaman)
            ->order_by('id_pengumuman','DESC')
            ->get();

        if($query->num_rows() > 0)
        {
            return $query;
        }
        else
        {
            return NULL;
        }
    }

    //detail pengumuman
    function detail_pengumuman($slug)
    {
        $query = $this->db->select('a.id_pengumuman, a.judul_pengumuman, a.slug_pengumuman, a.user_id, a.isi_pengumuman, a.gambar, a.tags, a.descriptions, a.created_at, b.id_user, b.nama_user')
            ->from('tbl_pengumuman a')
            ->join('tbl_users b', 'a.user_id = b.id_user')
            ->where('a.slug_pengumuman', $slug)
            ->get();

        if($query->num_rows() > 0)
        {
            return $query->row();
        }else
        {
            return NULL;
        }
    }

    //count album
    function count_album()
    {
        $query = $this->db->select('*')
            ->from('tbl_album')
            ->order_by('id_album','DESC')
            ->get();

        if($query->num_rows() > 0)
        {
            return $query->num_rows();
        }
        else
        {
            return NULL;
        }
    }

    //index album
    function index_album($halaman, $batas)
    {
        $query = $this->db->select('*')
            ->from('tbl_album')
            ->limit($batas, $halaman)
            ->order_by('id_album','DESC')
            ->get();

        if($query->num_rows() > 0)
        {
            return $query;
        }
        else
        {
            return NULL;
        }
    }
    
    //detail album
    public function detail_album($slug)
    {
        $query = $this->db->query("SELECT a.id_foto, a.album_id, a.caption, a.foto, b.id_album, b.nama_album, b.slug_album FROM tbl_foto a LEFT JOIN tbl_album b ON a.album_id = b.id_album WHERE b.slug_album = '$slug' ORDER BY a.id_foto DESC");
        return $query;
    }

    //detail album row
    public function detail_album_row($slug)
    {
        $query = $this->db->query("SELECT a.id_foto, a.album_id, a.caption, a.foto, b.id_album, b.nama_album, b.slug_album FROM tbl_foto a LEFT JOIN tbl_album b ON a.album_id = b.id_album WHERE b.slug_album = '$slug'");
        
        if($query->num_rows() > 0)
        {
            return $query->row();
        }else
        {
            return NULL;
        }
    }

    //get all slider
    public function get_all_slider()
    {
        $query =  $this->db->order_by('id_slider','DESC')->get('tbl_sliders');
        return $query;
    }

    //get  berita one
    public function get_berita_one()
    {
        $query = $this->db->select('a.id_berita, a.kategori_id, a.user_id, a.slug_berita, a.judul_berita, a.descriptions, a.gambar, a.created_at, b.id_kategori, b.nama_kategori, c.id_user, c.nama_user')
            ->from('tbl_berita a')
            ->join('tbl_kategori b', 'a.kategori_id = b.id_kategori')
            ->join('tbl_users c', 'a.user_id = c.id_user')
            ->order_by('a.created_at','DESC')
            ->limit(1,0)
            ->get();
        return $query;
    }

    //get  berita two
    public function get_berita_two()
    {
        $query = $this->db->select('a.id_berita, a.kategori_id, a.user_id, a.slug_berita, a.judul_berita, a.descriptions, a.gambar, a.created_at, b.id_kategori, b.nama_kategori, c.id_user, c.nama_user')
            ->from('tbl_berita a')
            ->join('tbl_kategori b', 'a.kategori_id = b.id_kategori')
            ->join('tbl_users c', 'a.user_id = c.id_user')
            ->order_by('a.created_at','DESC')
            ->limit(1,1)
            ->get();
        return $query;
    }

    //get  berita three
    public function get_berita_three()
    {
        $query = $this->db->select('a.id_berita, a.kategori_id, a.user_id, a.slug_berita, a.judul_berita, a.descriptions, a.gambar, a.created_at, b.id_kategori, b.nama_kategori, c.id_user, c.nama_user')
            ->from('tbl_berita a')
            ->join('tbl_kategori b', 'a.kategori_id = b.id_kategori')
            ->join('tbl_users c', 'a.user_id = c.id_user')
            ->order_by('a.created_at','DESC')
            ->limit(6,2)
            ->get();
        return $query;
    }

    //get all berita
    public function get_all_berita()
    {
        $query = $this->db->select('a.id_berita, a.kategori_id, a.user_id, a.slug_berita, a.judul_berita, a.descriptions, a.gambar, a.created_at, b.id_kategori, b.nama_kategori, c.id_user, c.nama_user')
            ->from('tbl_berita a')
            ->join('tbl_kategori b', 'a.kategori_id = b.id_kategori')
            ->join('tbl_users c', 'a.user_id = c.id_user')
            ->order_by('a.created_at','DESC')
            ->limit(6,0)
            ->get();
        return $query;
    }

    //get  agenda one
    public function get_agenda_one()
    {
        $query = $this->db->select('*')
            ->from('tbl_agenda')
            ->order_by('id_agenda','DESC')
            ->limit(2,0)
            ->get();
        return $query;
    }

    //get  agenda three
    public function get_agenda_three()
    {
        $query = $this->db->select('*')
            ->from('tbl_agenda')
            ->order_by('id_agenda','DESC')
            ->limit(8,2)
            ->get();
        return $query;
    }

    //get  agenda three
    public function get_agenda_four()
    {
        $query = $this->db->select('*')
            ->from('tbl_agenda')
            ->order_by('id_agenda','DESC')
            ->limit(6,5)
            ->get();
        return $query;
    }

    //get all agenda
    public function get_all_agenda()
    {
        $query = $this->db->select('*')
            ->from('tbl_agenda')
            ->order_by('id_agenda','DESC')
            ->limit(3,0)
            ->get();
        return $query;
    }

    //count search agenda
    function count_search_agenda($keyword)
    {
        $query = $this->db->like('judul_agenda',$keyword)->get('tbl_agenda');

        if($query->num_rows() > 0)
        {
            return $query->num_rows();
        }
        else
        {
            return NULL;
        }
    }

    //index search agenda
    public function index_search_agenda($keyword,$limit,$offset)
    {
        $query = $this->db->select('*')
            ->from('tbl_agenda')
            ->limit($limit,$offset)
            ->like('judul_agenda',$keyword)
            ->limit($limit,$offset)
            ->order_by('id_agenda','DESC')
            ->get();

        if($query->num_rows() > 0)
        {
            return $query;
        }
        else
        {
            return NULL;
        }
    }

    //count search berita
    function count_search_berita($keyword)
    {
        $query = $this->db->like('judul_berita',$keyword)->get('tbl_berita');

        if($query->num_rows() > 0)
        {
            return $query->num_rows();
        }
        else
        {
            return NULL;
        }
    }

    //index search berita
    public function index_search_berita($keyword,$limit,$offset)
    {
        $query = $this->db->select('*')
            ->from('tbl_berita')
            ->limit($limit,$offset)
            ->like('judul_berita',$keyword)
            ->limit($limit,$offset)
            ->order_by('id_berita','DESC')
            ->get();

        if($query->num_rows() > 0)
        {
            return $query;
        }
        else
        {
            return NULL;
        }
    }

    //count search pengumuman
    function count_search_pengumuman($keyword)
    {
        $query = $this->db->like('judul_pengumuman',$keyword)->get('tbl_pengumuman');

        if($query->num_rows() > 0)
        {
            return $query->num_rows();
        }
        else
        {
            return NULL;
        }
    }

    //index search pengumuman
    public function index_search_pengumuman($keyword,$limit,$offset)
    {
        $query = $this->db->select('*')
            ->from('tbl_pengumuman')
            ->limit($limit,$offset)
            ->like('judul_pengumuman',$keyword)
            ->limit($limit,$offset)
            ->order_by('id_pengumuman','DESC')
            ->get();

        if($query->num_rows() > 0)
        {
            return $query;
        }
        else
        {
            return NULL;
        }
    }

    //get pengumuman one
    public function get_pengumuman_one()
    {
        $query = $this->db->select('*')
            ->from('tbl_pengumuman')
            ->order_by('id_pengumuman','DESC')
            ->limit(3,0)
            ->get();
        return $query;
    }

    //get pengumuman three
    public function get_pengumuman_three()
    {
        $query = $this->db->select('*')
            ->from('tbl_pengumuman')
            ->order_by('id_pengumuman','DESC')
            ->limit(3,1)
            ->get();
        return $query;
    }

    //get all pengumuman
    public function get_all_pengumuman()
    {
        $query = $this->db->select('*')
            ->from('tbl_pengumuman')
            ->order_by('id_pengumuman','DESC')
            ->limit(3,0)
            ->get();
        return $query;
    }

    //get all download 
    public function get_all_download()
    {
        $query = $this->db->select('*')
        ->from('tbl_file a')
        ->join('tbl_users b', 'a.user_id = b.id_user')
        ->order_by('a.id_file','DESC')
        ->get();
    return $query->result();
    }

    //get all guru
    public function get_all_guru()
    {
        $query = $this->db->select('*')
            ->from('tbl_guru')
            ->order_by('id_guru','DESC')
            ->get();
        return $query->result();
    }

    //detail guru
    function detail_guru($nip)
    {
        $query = $this->db->select('*')
            ->from('tbl_guru')
            ->where('nip', $nip)
            ->get();

        if($query->num_rows() > 0)
        {
            return $query->row();
        }else
        {
            return NULL;
        }
    }

    //count video
    function count_video()
    {
        $query = $this->db->select('*')
            ->from('tbl_video')
            ->order_by('id_video','DESC')
            ->get();

        if($query->num_rows() > 0)
        {
            return $query->num_rows();
        }
        else
        {
            return NULL;
        }
    }

    //index video
    function index_video($halaman, $batas)
    {
        $query = $this->db->select('*')
            ->from('tbl_video')
            ->limit($batas, $halaman)
            ->order_by('id_video','DESC')
            ->get();

        if($query->num_rows() > 0)
        {
            return $query;
        }
        else
        {
            return NULL;
        }
    }

    //get all kelas
    public function get_all_kelas()
    {
        $query = $this->db->select('*')
            ->from('tbl_kelas')
            ->order_by('id_kelas','DESC')
            ->get();
        return $query;
    }

    //detail siswa
    public function detail_siswa($slug)
    {
        $query = $this->db->select('*')
                ->from('tbl_siswa a')
                ->join('tbl_kelas b', 'a.kelas_id = b.id_kelas')
                ->order_by('a.nama_siswa','DESC')
                ->where('b.slug', $slug)
                ->order_by('nama_siswa', 'ASC')
                ->get();
        return $query->result();
    }

    //detail album row
    public function detail_kelas_row($slug)
    {
        $query = $this->db->query("SELECT * FROM tbl_siswa a LEFT JOIN tbl_kelas b ON a.kelas_id = b.id_kelas WHERE b.slug = '$slug'");
        
        if($query->num_rows() > 0)
        {
            return $query->row();
        }else
        {
            return NULL;
        }
    }

    //detail menu
    public function detail_menu($slug)
    {
        $query = $this->db->select('a.id_menu, a.judul_menu, a.slug, a.user_id, a.content, a.tags, a.descriptions, a.updated_at, b.id_user, b.nama_user')
            ->from('tbl_menu a')
            ->join('tbl_users b', 'a.user_id = b.id_user')
            ->where('a.slug', $slug)
            ->get();

        if($query->num_rows() > 0)
        {
            return $query->row();
        }else
        {
            return NULL;
        }
    }

    //detail album row
    public function detail_menu_row($slug)
    {
        $query = $this->db->select('*')
                ->from('tbl_menu a')
                ->join('tbl_users b', 'a.user_id = b.id_user')
                ->where('a.slug', $slug)
                ->get();
        
        if($query->num_rows() > 0)
        {
            return $query->row();
        }else
        {
            return NULL;
        }
    }

    function get_sitemap_berita()
    {
        $query  =   $this->db->order_by("id_berita","DESC")->get("tbl_berita");
        return $query->result_array();
    }

    function get_sitemap_agenda()
    {
        $query  =   $this->db->order_by("id_agenda","DESC")->get("tbl_agenda");
        return $query->result_array();
    }

    function get_sitemap_pengumuman()
    {
        $query  =   $this->db->order_by("id_pengumuman","DESC")->get("tbl_pengumuman");
        return $query->result_array();
    }

    function count_kategori($slug)
    {
        $query = $this->db->select('*')
            ->from('tbl_berita a')
            ->join('tbl_kategori b', 'a.kategori_id = b.id_kategori')
            ->where('b.slug',$slug)
            ->order_by('a.id_berita','DESC')
            ->get();

        if($query->num_rows() > 0)
        {
            return $query->num_rows();
        }
        else
        {
            return NULL;
        }
    }

    function get_kategori_judul($slug)
    {
        $query = $this->db->select('*')
            ->from('tbl_kategori')
            ->where('slug',$slug)
            ->get();

        if($query->num_rows() > 0)
        {
            return $query->row();
        }
        else
        {
            return NULL;
        }
    }

    function index_kategori($slug,$limit,$offset)
    {
        $query = $this->db->select('*')
            ->from('tbl_berita a')
            ->join('tbl_kategori b', 'a.kategori_id = b.id_kategori')
            ->where('b.slug',$slug)
            ->limit($offset,$limit)
            ->order_by('a.id_berita','DESC')
            ->get();

        if($query->num_rows() > 0)
        {
            return $query;
        }
        else
        {
            return NULL;
        }
    }

    public function counter_visitor()
    {
        setcookie("pengunjung", "sudah berkunjung", time()+60*60*24);
        if (!isset($_COOKIE["pengunjung"])) {
            $d_in['ip_address'] = $_SERVER['REMOTE_ADDR'];
            $d_in['date_visit'] = date("Y-m-d H:i:s");
            //$this->db->insert("tbl_counter",$d_in);
        }
    }

    //fungsi date time
    function tgl_time_indo($date = null)
    {
        $tglindo = date("d-m-Y H:i:s", strtotime($date));
        $formatTanggal = $tglindo;
        return $formatTanggal;
    }

    function tgl_database($date = null)
    {
        $tgldatabase = date("Y-m-d", strtotime($date));
        $formatTanggal = $tgldatabase;
        return $formatTanggal;
    }

    function tgl_indo($date = null)
    {
        $tglindo = date("d-m-Y", strtotime($date));
        $formatTanggal = $tglindo;
        return $formatTanggal;
    }

    function tgl_tunggal($date = null)
    {
        $tglindo = date("j", strtotime($date));
        $formatTanggal = $tglindo;
        return $formatTanggal;
    }

    function tgl_mont_year($date = null)
    {
        $tglindo = date("n, Y");
        return $tglindo;
    }

    function year_tunggal($date = null)
    {
        $tglindo = date("Y");
        return $tglindo;
    }

    function jam_format($time = null)
    {
        $jamformat = date("H:i", strtotime($time));
        $formatJam = $jamformat;
        return $formatJam;
    }

    function bulan_inggris($date = null)
    {
        //buat array nama hari dalam bahasa Indonesia dengan urutan 1-7
        $array_hari = array(1 => 'Senin', 'Selasa', 'Rabu', 'Kamis', 'Jumat', 'Sabtu', 'Minggu');
        //buat array nama bulan dalam bahasa Indonesia dengan urutan 1-12
        $array_bulan = array(1 => 'Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug',
            'Sep', 'Oct', 'Nov', 'Dec');
        if ($date == null) {
            //jika $date kosong, makan tanggal yang diformat adalah tanggal hari ini
            $hari = $array_hari[date('N')];
            $tanggal = date('j');
            $bulan = $array_bulan[date('n')];
            $tahun = date('Y');
        } else {
            //jika $date diisi, makan tanggal yang diformat adalah tanggal tersebut
            $date = strtotime($date);
            $hari = $array_hari[date('N', $date)];
            $tanggal = date('j', $date);
            $bulan = $array_bulan[date('n', $date)];
            $tahun = date('Y', $date);
        }
        $formatTanggal = $bulan;
        return $formatTanggal;
    }

    function tgl_indo_no_hari($date = null)
    {
        //buat array nama hari dalam bahasa Indonesia dengan urutan 1-7
        $array_hari = array(1 => 'Senin', 'Selasa', 'Rabu', 'Kamis', 'Jumat', 'Sabtu', 'Minggu');
        //buat array nama bulan dalam bahasa Indonesia dengan urutan 1-12
        $array_bulan = array(1 => 'Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus',
            'September', 'Oktober', 'November', 'Desember');
        if ($date == null) {
            //jika $date kosong, makan tanggal yang diformat adalah tanggal hari ini
            $hari = $array_hari[date('N')];
            $tanggal = date('j');
            $bulan = $array_bulan[date('n')];
            $tahun = date('Y');
        } else {
            //jika $date diisi, makan tanggal yang diformat adalah tanggal tersebut
            $date = strtotime($date);
            $hari = $array_hari[date('N', $date)];
            $tanggal = date('j', $date);
            $bulan = $array_bulan[date('n', $date)];
            $tahun = date('Y', $date);
        }
        $formatTanggal = $tanggal . " " . $bulan . " " . $tahun;
        return $formatTanggal;
    }

    function bulan_indo($date = null)
    {
        //buat array nama hari dalam bahasa Indonesia dengan urutan 1-7
        $array_hari = array(1 => 'Senin', 'Selasa', 'Rabu', 'Kamis', 'Jumat', 'Sabtu', 'Minggu');
        //buat array nama bulan dalam bahasa Indonesia dengan urutan 1-12
        $array_bulan = array(1 => 'Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus',
            'September', 'Oktober', 'November', 'Desember');
        if ($date == null) {
            //jika $date kosong, makan tanggal yang diformat adalah tanggal hari ini
            $hari = $array_hari[date('N')];
            $tanggal = date('j');
            $bulan = $array_bulan[date('n')];
            $tahun = date('Y');
        } else {
            //jika $date diisi, makan tanggal yang diformat adalah tanggal tersebut
            $date = strtotime($date);
            $hari = $array_hari[date('N', $date)];
            $tanggal = date('j', $date);
            $bulan = $array_bulan[date('n', $date)];
            $tahun = date('Y', $date);
        }
        $formatTanggal = $bulan;
        return $formatTanggal;
    }

    function tgl_indo_lengkap($date = null)
    {
        //buat array nama hari dalam bahasa Indonesia dengan urutan 1-7
        $array_hari = array(1 => 'Senin', 'Selasa', 'Rabu', 'Kamis', 'Jumat', 'Sabtu', 'Minggu');
        //buat array nama bulan dalam bahasa Indonesia dengan urutan 1-12
        $array_bulan = array(1 => 'Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus',
            'September', 'Oktober', 'November', 'Desember');
        if ($date == null) {
            //jika $date kosong, makan tanggal yang diformat adalah tanggal hari ini
            $hari = $array_hari[date('N')];
            $tanggal = date('d');
            $bulan = $array_bulan[date('n')];
            $tahun = date('Y');
            $jam = date('H:i:s');
        } else {
            //jika $date diisi, makan tanggal yang diformat adalah tanggal tersebut
            $date = strtotime($date);
            $hari = $array_hari[date('N', $date)];
            $tanggal = date('d', $date);
            $bulan = $array_bulan[date('n', $date)];
            $tahun = date('Y', $date);
            $jam = date('H:i:s', $date);
        }
        $formatTanggal = $hari . ", " . $tanggal . " " . $bulan . " " . $tahun . " Jam " . $jam;
        return $formatTanggal;
    }

    function tgl_jam_indo_no_hari($date = null)
    {
        //buat array nama hari dalam bahasa Indonesia dengan urutan 1-7
        $array_hari = array(1 => 'Senin', 'Selasa', 'Rabu', 'Kamis', 'Jumat', 'Sabtu', 'Minggu');
        //buat array nama bulan dalam bahasa Indonesia dengan urutan 1-12
        $array_bulan = array(1 => 'Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus',
            'September', 'Oktober', 'November', 'Desember');
        if ($date == null) {
            //jika $date kosong, makan tanggal yang diformat adalah tanggal hari ini
            $hari = $array_hari[date('N')];
            $tanggal = date('d');
            $bulan = $array_bulan[date('n')];
            $tahun = date('Y');
            $jam = date('H:i:s');
        } else {
            //jika $date diisi, makan tanggal yang diformat adalah tanggal tersebut
            $date = strtotime($date);
            $hari = $array_hari[date('N', $date)];
            $tanggal = date('d', $date);
            $bulan = $array_bulan[date('n', $date)];
            $tahun = date('Y', $date);
            $jam = date('H:i:s', $date);
        }
        $formatTanggal = $tanggal . " " . $bulan . " " . $tahun . " Jam " . $jam;
        return $formatTanggal;
    }

    //fungsi date ago
    function time_elapsed_string($datetime, $full = false)
    {
        $today = time();
        $createdday = strtotime($datetime);
        $datediff = abs($today - $createdday);
        $difftext = "";
        $years = floor($datediff / (365 * 60 * 60 * 24));
        $months = floor(($datediff - $years * 365 * 60 * 60 * 24) / (30 * 60 * 60 * 24));
        $days = floor(($datediff - $years * 365 * 60 * 60 * 24 - $months * 30 * 60 * 60 * 24) / (60 * 60 * 24));
        $hours = floor($datediff / 3600);
        $minutes = floor($datediff / 60);
        $seconds = floor($datediff);
        //year checker
        if ($difftext == "") {
            if ($years > 1)
                $difftext = $years . " years ago";
            elseif ($years == 1)
                $difftext = $years . " year ago";
        }
        //month checker
        if ($difftext == "") {
            if ($months > 1)
                $difftext = $months . " months ago";
            elseif ($months == 1)
                $difftext = $months . " month ago";
        }
        //month checker
        if ($difftext == "") {
            if ($days > 1)
                $difftext = $days . " days ago";
            elseif ($days == 1)
                $difftext = $days . " day ago";
        }
        //hour checker
        if ($difftext == "") {
            if ($hours > 1)
                $difftext = $hours . " hours ago";
            elseif ($hours == 1)
                $difftext = $hours . " hour ago";
        }
        //minutes checker
        if ($difftext == "") {
            if ($minutes > 1)
                $difftext = $minutes . " minutes ago";
            elseif ($minutes == 1)
                $difftext = $minutes . " minute ago";
        }
        //seconds checker
        if ($difftext == "") {
            if ($seconds > 1)
                $difftext = $seconds . " seconds ago";
            elseif ($seconds == 1)
                $difftext = $seconds . " second ago";
        }
        return $difftext;
    }

}
