<?php
class Ppdb extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		$this->load->model('web');
		$this->load->model('m_pages');
	}
	function index()
	{
		$data['data'] = $this->db->query("SELECT * FROM tbl_info_ppdb WHERE id=1")->row();
		$data['menu'] = $this->db->query("SELECT * FROM tbl_menu_ppdb")->result();
		$this->load->view('public/header');
		$this->load->view('public/ppdb/index', $data);
		$this->load->view('public/footer');
	}

	public function detail($slugs)
    {
        $slug   = htmlspecialchars($slugs, ENT_QUOTES);
        $query  = $this->db->query("SELECT * FROM tbl_page WHERE slug='$slug'");
        if ($query->num_rows() > 0) {
            $data       = $query->row_array();
            $id_halaman = $data['id'];

            $data_page['pages']     = $this->m_pages->get_pages($id_halaman);
            $data_page['title']     = $data['title'];
            $data_page['populer']   = $this->db->query("SELECT * FROM tbl_tulisan ORDER BY tulisan_views DESC LIMIT 5");
            $data_page['category']  = $this->db->get('tbl_kategori');
            $data_page['menu'] = $this->db->query("SELECT * FROM tbl_menu_ppdb")->result();
            $this->load->view('public/header');
            $this->load->view('public/pages/index', $data_page);
            $this->load->view('public/footer');
        } else {
            $this->load->view('errors/index');
        }
    }
}