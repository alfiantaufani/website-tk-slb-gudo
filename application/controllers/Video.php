<?php
class Video extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		$this->load->model('web');
	}
	function index()
	{
		$x['data'] = $this->db->query("SELECT * FROM tbl_video ORDER BY id DESC")->result();
		$this->load->view('public/header');
		$this->load->view('public/galeri/video', $x);
		$this->load->view('public/footer');
	}
}