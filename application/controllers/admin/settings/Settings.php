<?php
class Settings extends CI_Controller{
    function __construct(){
		parent::__construct();
		if($this->session->userdata('masuk') !=TRUE){
            $url=base_url('administrator');
            redirect($url);
        };
		$this->load->model('m_settings');
		$this->load->library('upload');
        $this->load->helper("file");
	}

    public function index()
    {
        $x['data'] = $this->m_settings->get_all_settings();
		$x['title'] = "Setting";
		$this->load->view('admin/settings/index',$x);
    }

    public function update()
    {
        if($this->input->post('type') == "homepage"){
            $config['upload_path'] = './theme/images/kepsek/'; //path folder
            $config['allowed_types'] = 'gif|jpg|png|jpeg|bmp'; //type yang dapat diakses bisa anda sesuaikan
            $config['encrypt_name'] = TRUE; //nama yang terupload nantinya

            $this->upload->initialize($config);
            if(!empty($_FILES['filefoto']['name']))
            {
                if ($this->upload->do_upload('filefoto'))
                {
                    $gbr = $this->upload->data();
                    $gambar = $gbr['file_name'];
                        $cek = $this->input->post('foto_kepsek');
                        $path='./theme/images/kepsek/'.$cek;
                        unlink($path);
                    $id = 1;
                    $data = [
                        'nama_kepsek' => $this->input->post('nama_kepsek'),
                        'foto_kepsek' => $gambar,
                        'text_home' => $this->input->post('text_home'),
                    ];
                    $this->db->where('id', $id);
                    $update = $this->db->update("tbl_setting", $data);
                }else{
                    echo $this->session->set_flashdata('msg','error');
                    redirect('admin/settings/settings');
                }
            }else{
                $id = 1;
                $data = [
                    'nama_kepsek' => $this->input->post('nama_kepsek'),
                    'text_home' => $this->input->post('text_home'),
                ];
                $this->db->where('id', $id);
                $update = $this->db->update("tbl_setting", $data);
            }

            if($update){
                echo $this->session->set_flashdata('msg','succes-update');
                redirect('admin/settings/settings');
            }else{
                echo $this->session->set_flashdata('msg','error');
                redirect('admin/settings/settings');
            }
            
        }else if($this->input->post('save_kontak') == "kontak"){
            $id = 1;
            $data = [
                'email' => $this->input->post('email'),
                'no_telephone' => $this->input->post('no_telephone'),
                'maps' => $this->input->post('maps'),
                'address' => $this->input->post('address'),
            ];
            $this->db->where('id', $id);
            $update_kontak = $this->db->update("tbl_setting", $data);
            if($update_kontak){
                echo $this->session->set_flashdata('msg','succes-update');
                redirect('admin/settings/settings');
            }else{
                echo $this->session->set_flashdata('msg','error');
                redirect('admin/settings/settings');
            }
        }else if($this->input->post('save_sosmed') == "sosmed"){
            $id = 1;
            $data = [
                'instagram' => $this->input->post('instagram'),
                'twitter' => $this->input->post('twitter'),
                'facebook' => $this->input->post('facebook'),
            ];
            $this->db->where('id', $id);
            $update_sosmed = $this->db->update("tbl_setting", $data);
            if($update_sosmed){
                echo $this->session->set_flashdata('msg','succes-update');
                redirect('admin/settings/settings');
            }else{
                echo $this->session->set_flashdata('msg','error');
                redirect('admin/settings/settings');
            }

        }else if($this->input->post('save_logo') == "logo"){
            $config['upload_path'] = './theme/images/logo/'; //path folder
            $config['allowed_types'] = 'gif|jpg|png|jpeg|bmp'; //type yang dapat diakses bisa anda sesuaikan
            $config['encrypt_name'] = TRUE; //nama yang terupload nantinya

            $this->upload->initialize($config);
            if(!empty($_FILES['filelogo']['name']))
            {
                if ($this->upload->do_upload('filelogo'))
                {
                    $gbr = $this->upload->data();
                    $gambar = $gbr['file_name'];
                    $id = 1;
                    $data = [
                        'logo' => $gambar,
                    ];
                    $this->db->where('id', $id);
                    $update_logo = $this->db->update("tbl_setting", $data);
                }else{
                    echo $this->session->set_flashdata('msg','error');
                    redirect('admin/settings/settings');
                }
            }

            if($update_logo){
                echo $this->session->set_flashdata('msg','succes-update');
                redirect('admin/settings/settings');
            }else{
                echo $this->session->set_flashdata('msg','error');
                redirect('admin/settings/settings');
            }
        }else if($this->input->post('save_sistem') == "sistem"){
            $id = 1;
            $data = [
                'admin_title' => $this->input->post('admin_title'),
                'admin_footer' => $this->input->post('admin_footer'),
                'site_title' => $this->input->post('site_title'),
                'site_footer' => $this->input->post('site_footer'),
            ];
            $this->db->where('id', $id);
            $update_sistem = $this->db->update("tbl_setting", $data);
            if($update_sistem){
                echo $this->session->set_flashdata('msg','succes-update');
                redirect('admin/settings/settings');
            }else{
                echo $this->session->set_flashdata('msg','error');
                redirect('admin/settings/settings');
            }
            
        }else if($this->input->post('save_seo') == "seo"){
            $id = 1;
            $data = [
                'description' => $this->input->post('description')
            ];
            $this->db->where('id', $id);
            $update_seo = $this->db->update("tbl_setting", $data);
            if($update_seo){
                echo $this->session->set_flashdata('msg','succes-update');
                redirect('admin/settings/settings');
            }else{
                echo $this->session->set_flashdata('msg','error');
                redirect('admin/settings/settings');
            }
        }

        
        // if($update){
        //     echo $this->session->set_flashdata('msg','succes-update');
        //     redirect('admin/settings/settings');
        // }else{
        //     echo $this->session->set_flashdata('msg','error');
        //     redirect('admin/settings/settings');
        // }
    }
}