<?php
class Video extends CI_Controller{
	function __construct(){
		parent::__construct();
		if($this->session->userdata('masuk') !=TRUE){
            $url=base_url('administrator');
            redirect($url);
        };
		$this->load->library('upload');
	}


	function index(){
        $x['title'] = 'Galeri Video';
		$x['data'] = $this->db->query("SELECT * FROM tbl_video ORDER BY id DESC")->result();
		$this->load->view('admin/video/index',$x);
	}

    public function create()
    {
        $x['title'] = 'Add Video';
        $this->load->view('admin/video/create', $x);
    }

    public function store()
    {
        $data = [
            'title' => $this->input->post('title'),
            'embed_youtube' => $this->input->post('embed_youtube'),
        ];
        $insert = $this->db->insert("tbl_video", $data);
        if($insert){
            echo $this->session->set_flashdata('msg','success');
            redirect('admin/video/index');
        }else{
            echo $this->session->set_flashdata('msg','error');
            redirect('admin/video/index');
        }
    }

    public function edit($id)
    {
        $x['title'] = 'Edit Galeri Video';
		$x['data'] = $this->db->query("SELECT * FROM tbl_video WHERE id='$id'")->row();
		$this->load->view('admin/video/edit',$x);
    }

    public function update()
    {
        $id = $this->input->post('id');
        $data = [
            'title' => $this->input->post('title'),
            'embed_youtube' => $this->input->post('embed_youtube'),
        ];
        $this->db->where('id', $id);
        $update = $this->db->update("tbl_video", $data);
        if($update){
            echo $this->session->set_flashdata('msg','succes-update');
            redirect('admin/video/index');
        }else{
            echo $this->session->set_flashdata('msg','error');
            redirect('admin/video/index');
        }
    }

    public function delete()
	{
		$kode 	= strip_tags($this->input->post('kode'));
		$hsl	= $this->db->query("DELETE FROM tbl_video WHERE id='$kode'");
		if($hsl){
			echo $this->session->set_flashdata('msg','success-hapus');
			redirect('admin/video/index');
		}else{
			var_dump("Gagal hapus");
		}
	}

}