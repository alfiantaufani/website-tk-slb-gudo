<?php
class Ppdb extends CI_Controller{
	function __construct(){
		parent::__construct();
		if($this->session->userdata('masuk') !=TRUE){
            $url=base_url('administrator');
            redirect($url);
        };
		$this->load->library('upload');
	}

	function index(){
        $x['title'] = 'Informasi PPDB';
		$x['data'] = $this->db->query("SELECT * FROM tbl_info_ppdb WHERE id=1")->row();
		$this->load->view('admin/ppdb/index',$x);
	}

    public function update()
    {
        $data = [
            'description' => $this->input->post('description')
        ];

        $this->db->where('id', $this->input->post('id'));
        $update = $this->db->update("tbl_info_ppdb", $data);
        if($update){
            echo $this->session->set_flashdata('msg','succes-update');
            redirect('admin/ppdb/index');
        }else{
            echo $this->session->set_flashdata('msg','error');
            redirect('admin/ppdb/index');
        }
    }

    public function menu_ppdb()
    {
        $x['title'] = 'Menu PPDB';
		$x['data'] = $this->db->query("SELECT * FROM tbl_menu_ppdb")->result();
		$this->load->view('admin/ppdb/menu',$x);
    }

    public function menu_create()
    {
        $x['title'] = 'Add Menu PPDB';
        $this->load->view('admin/ppdb/menu_create', $x);
    }

    public function menu_store()
    {
        $data = [
            'menu_name' => $this->input->post('menu_name'),
            'menu_link' => $this->input->post('menu_link'),
        ];
        $insert = $this->db->insert("tbl_menu_ppdb", $data);
        if($insert){
            echo $this->session->set_flashdata('msg','success');
            redirect('admin/ppdb/menu_ppdb');
        }else{
            echo $this->session->set_flashdata('msg','error');
            redirect('admin/ppdb/menu_ppdb');
        }
    }

    public function menu_edit($id)
    {
        $x['title'] = 'Edit Menu PPDB';
		$x['data'] = $this->db->query("SELECT * FROM tbl_menu_ppdb WHERE id='$id'")->row();
		$this->load->view('admin/ppdb/menu_edit',$x);
    }

    public function menu_delete()
    {
        $kode 	= strip_tags($this->input->post('kode'));
		$hsl	= $this->db->query("DELETE FROM tbl_menu_ppdb WHERE id='$kode'");
		if($hsl){
			echo $this->session->set_flashdata('msg','success-hapus');
			redirect('admin/ppdb/menu_ppdb');
		}else{
			var_dump("Gagal hapus");
		}
    }
}