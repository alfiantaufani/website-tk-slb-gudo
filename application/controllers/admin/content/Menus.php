<?php
class Menus extends CI_Controller{
    function __construct(){
		parent::__construct();
		if($this->session->userdata('masuk') !=TRUE){
            $url=base_url('administrator');
            redirect($url);
        };
		$this->load->model('m_menus');
	}

	public function index(){
		$x['data'] = $this->m_menus->get_all_menus();
		$x['title'] = "Menu Utama";
		$this->load->view('admin/menus/index',$x);
	}

	public function create()
	{
		$x['title'] = "Tambah Menu Utama";
		$this->load->view('admin/menus/create',$x);
	}

	public function store()
	{
		$nama_menu = $this->input->post('nama_menu');
		$cek = $this->db->query("SELECT * FROM tbl_menu WHERE menu_name='$nama_menu' LIMIT 1");
		if($cek->num_rows() > 0){
			$this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">
				<b>Gagal!</b> Nama Menu sudah ada <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button></div>
			');
			$referred_from = $this->session->userdata('referred_from'); 
			redirect('admin/content/menus/create');
		}else{
			$data = [
				'menu_name' => $nama_menu,
				'menu_link' => $this->input->post('link_menu'),
			];
			$insert = $this->db->insert("tbl_menu", $data);
			if($insert){
				echo $this->session->set_flashdata('msg','success');
				redirect('admin/content/menus');
			}else{
				echo $this->session->set_flashdata('msg','error');
				redirect('admin/content/menus');
			}
		}
	}

	public function edit($id)
	{
		$data['title'] = "Edit Menu Utama";
		$data['menus'] = $this->m_menus->get_menus($id);
		$this->load->view('admin/menus/edit',$data);
	}

	public function update()
	{
		$id = $this->input->post('id');
		$data = [
			'menu_name' => $this->input->post('nama_menu'),
			'menu_link' => $this->input->post('link_menu'),
		];
		$this->db->where('id', $id);
		$update = $this->db->update("tbl_menu", $data);
		if($update){
			echo $this->session->set_flashdata('msg','success');
			redirect('admin/content/menus');
		}else{
			echo $this->session->set_flashdata('msg','error');
			redirect('admin/content/menus');
		}
	}

	public function delete()
	{
		$kode 	= strip_tags($this->input->post('kode'));
		$hsl	= $this->db->query("DELETE FROM tbl_menu WHERE id='$kode'");
		if($hsl){
			echo $this->session->set_flashdata('msg','success-hapus');
			redirect('admin/content/menus');
		}else{
			var_dump("Gagal hapus");
		}
	}
}