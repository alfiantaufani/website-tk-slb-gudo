<?php
class Galeri extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		$this->load->model('m_galeri');
		$this->load->model('m_album');
		$this->load->model('web');
	}
	function index()
	{
		$x['alb'] = $this->m_album->get_all_album();
		$x['all_galeri'] = $this->m_galeri->get_all_galeri();
		$this->load->view('public/header');
		$this->load->view('public/galeri/index', $x);
		$this->load->view('public/footer');
	}
	function detail($id)
	{
		$x['nama_album'] = $this->db->query("SELECT album_nama FROM tbl_album WHERE album_id='$id'")->row();
		$x['data'] = $this->db->query("SELECT * FROM tbl_galeri WHERE galeri_album_id='$id'")->result();
		$this->load->view('public/header');
		$this->load->view('public/galeri/detail', $x);
		$this->load->view('public/footer');
	}
}
