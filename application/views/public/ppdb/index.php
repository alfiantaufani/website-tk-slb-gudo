<div class="container">
    <div class="row">
        <!-- content kiri -->
        <div class="col-xs-12 col-md-12 mt-3">
            <div class="text-center mt-4 mb-5">
                <h2 class="font-weight-bold ">PPDB</h2>
                <p class="text-muted">Berikut Halaman PPDB <?php echo systems('site_title') ?></p>
            </div>
            <!-- berita baru -->
        </div>

        <div class="col-md-3">
            <div class="card shadow">
                <div class="card-body">
                    <label><i class="fa fa-home"></i> MAIN MENU</label>
                    <ul class="list-group list-group-flush">
                        <li class="list-group-item"><a href="<?= base_url() ?>ppdb/" class="text-decoration-none">PPDB</a> </li>
                        <?php
                        if ($menu != NULL) :
                            foreach ($menu as $hasil) :
                        ?>
                                <li class="list-group-item"><a href="<?= base_url() ?>ppdb/<?= $hasil->menu_link ?>" class="text-decoration-none"><?= $hasil->menu_name ?></a> </li>
                            <?php
                            endforeach;
                        else :
                            ?>
                            Foto Belum Tersedia
                        <?php endif; ?>
                    </ul>
                </div>
            </div>
        </div>
        <div class="col-md-9">
            <div class="card shadow">
                <div class="card-body">
                    <div class="card-title">
                        <label><i class="fa fa-bell"></i> INFORMASI</label>
                        <hr>
                    </div>
                    <?= $data->description ?>
                </div>
            </div>
        </div>
    </div>
</div>