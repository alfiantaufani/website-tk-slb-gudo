<div class="container">
    <div class="row">
        <!-- content kiri -->
        <div class="col-xs-12 col-md-12 mt-3">
            <div class="text-center mt-4 mb-5">
                <h2 class="font-weight-bold ">Kontak Kami</h2>
                <p class="text-muted">Berikut Halaman Kontak Kami <?php echo systems('site_title') ?></p>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="card" style="width:100%">
                <div class="card-body" style="padding:10px">
                    <?php echo systems('maps') ?>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <!-- content kiri -->
        <div class="col-xs-12 col-md-7">
            <?php echo $this->session->flashdata('notif') ?>
            <div class="card" style="width:100%">
                <div class="card-body">
                    <div class="kontak">
                        <form action="<?php echo site_url('contact/kirim_pesan'); ?>" method="post">
                            <div class="form-group">
                                <input type="text" class="form-control" placeholder="Name" name="xnama" required>
                            </div>
                            <!-- // end .form-group -->
                            <div class="form-group">
                                <input type="email" class="form-control" placeholder="Email" name="xemail" required>
                            </div>
                            <!-- // end .form-group -->
                            <div class="form-group">
                                <input type="text" class="form-control" placeholder="Phone" name="xphone" required>
                            </div>
                            <!-- // end .form-group -->
                            <div class="form-group">
                                <textarea placeholder="Message" class="form-control" name="xmessage" required rows="5"></textarea>
                            </div>
                            <!-- // end .form-group -->
                            <button type="submit" class="btn btn-default btn-submit">SUBMIT</button>
                            <div><?php echo $this->session->flashdata('msg'); ?></div>
                            <!-- // end #js-contact-result -->
                        </form>
                    </div>
                </div>
            </div>
            <!-- berita baru -->

        </div>
        <!-- end content kiri -->

        <!-- content kanan -->
        <div class="col-xs-12 col-md-5">
            <div class="card" style="width:100%">
                <div class="card-body">
                    <label><i class="fa fa-map-marker"></i> ALAMAT SEKOLAH</label>
                    <p class="text-muted"><?php echo systems('address') ?></p>
                    <hr>
                    <label><i class="fa fa-envelope"></i> ALAMAT EMAIL</label>
                    <p class="text-muted"><?php echo systems('email') ?></p>
                    <hr>
                    <label><i class="fa fa-phone"></i> TELEPHONE</label>
                    <p class="text-muted"><?php echo systems('no_telephone') ?></p>
                </div>
            </div>
            <!-- end content kanan -->
        </div>
    </div>
</div>