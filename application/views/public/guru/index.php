<div class="container">
    <div class="row">
        <!-- content kiri -->
        <div class="col-xs-12 col-md-12 mt-3">
            <div class="text-center mt-4 mb-5">
                <h2 class="font-weight-bold ">Data Guru</h2>
                <p class="text-muted">Berikut Halaman Data Guru <?php echo systems('site_title') ?></p>
            </div>
            <!-- berita baru -->

            <div class="row">
                <?php
                if ($data != NULL) :
                    foreach ($data->result() as $hasil) :
                ?>
                        <div class="col-md-3">
                            <div class="card border-radius-10 shadow border-0">
                                <?php if (empty($hasil->guru_photo)) : ?>
                                    <img src="<?php echo base_url() . 'assets/images/blank.png'; ?>" class="img-fluid" alt="#">
                                <?php else : ?>
                                    <img class="card-img-top" src="<?php echo base_url() . 'assets/images/' . $hasil->guru_photo; ?>" alt="Card image cap">
                                <?php endif; ?>
                                <div class="card-body">
                                    <div class="text-center">
                                        <p class="font-wight-200"><b> <?php echo $hasil->guru_nama ?> </b></p>
                                        <p class="text-muted"> <?php echo $hasil->guru_mapel ?> </p>
                                        <small class="text-muted">NIP. <?php echo $hasil->guru_nip ?> </small>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php
                    endforeach;
                else :
                    ?>
                    Tidak Berita Disini
                <?php endif; ?>
                <div class="col-md-12 text-center mt-4">
                    <?php
                    error_reporting(0);
                    echo $page;
                    ?>
                </div>
            </div>
        </div>
        <!-- end content kiri -->


    </div>
</div>