<div class="container">
    <div class="row">
        <!-- content kiri -->
        <div class="col-xs-12 col-md-12 mt-3">
            <div class="text-center mt-4 mb-5">
                <h2 class="font-weight-bold ">Download</h2>
                <p class="text-muted">Berikut Halaman Download <?php echo systems('site_title') ?></p>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="card" style="width:100%">
                <div class="card-body" style="padding:10px">
                    <div class="table-responsive">
                        <table class="table table-striped" id="display">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Files</th>
                                    <th>Tanggal</th>
                                    <th>Oleh</th>
                                    <th style="text-align:center;">Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $no = 1;
                                foreach ($data->result() as $row) :
                                ?>
                                    <tr>
                                        <td><?php echo $no++; ?></td>
                                        <td><?php echo $row->file_judul; ?></td>
                                        <td><?php echo $row->tanggal; ?></td>
                                        <td><?php echo $row->file_oleh; ?></td>
                                        <td style="text-align:right;"><a href="<?php echo site_url('download/get_file/' . $row->file_id); ?>" class="btn btn-info">Download</a></td>
                                    </tr>
                                <?php endforeach; ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>