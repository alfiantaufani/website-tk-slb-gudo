<div class="footer" style="background:<?php echo theme_color_footer(); ?>;font-weight: 400">
  <div class="container">
    <div class="row">
      <div class="col-md-4">
        <h5><i class="fa fa-building-o"></i> TENTANG SEKOLAH</h5>
        <div class="divider-sekolah"></div>
        <p>
          <?php echo systems('description') ?>
        </p>
      </div>
      <div class="col-md-2">
        <h5><i class="fa fa-graduation-cap"></i> AKADEMIK</h5>
        <div class="divider-sekolah"></div>
        <ul>
          <li><a style="color:#fff;text-decoration:none;" href="<?php echo base_url() ?>guru/"><i class="fa fa-address-card-o"></i> Data Guru</a></li>
          <!--            <li><a style="color:#fff;text-decoration:none;" href="--><?php //echo base_url() 
                                                                                    ?>
          <!--siswa/"><i class="fa fa-child"></i> Data Siswa</a></li>-->
          <li><a style="color:#fff;text-decoration:none;" href="<?php echo base_url() ?>download/"><i class="fa fa-cloud-download"></i> Download</a></li>
        </ul>
      </div>
      <div class="col-md-2">
        <h5><i class="fa fa-clone"></i> MAIN MENU</h5>
        <div class="divider-sekolah"></div>
        <ul>
          <li><a style="color:#fff;text-decoration:none;" href="<?php echo base_url() ?>artikel/"><i class="fa fa-book"></i> Berita</a></li>
          <li><a style="color:#fff;text-decoration:none;" href="<?php echo base_url() ?>agenda/"><i class="fa fa-calendar-o"></i> Agenda</a></li>
          <li><a style="color:#fff;text-decoration:none;" href="<?php echo base_url() ?>pengumuman/"><i class="fa fa-bell-o"></i> Pengumuman</a></li>
        </ul>
      </div>
      <div class="col-md-4">
        <h5><i class="fa fa-phone"></i> HUBUNGI KAMI</h5>
        <div class="divider-sekolah"></div>
        <p style="line-height:30px">
          <i class="fa fa-map-marker"></i> <?php echo systems('address') ?><br>
          <i class="fa fa-phone"></i> <?php echo systems('no_telephone') ?><br>
        </p>
      </div>
    </div>
  </div>
</div>
<div class="copyright" style="background:<?php echo theme_color_copyright() ?>">
  <p><a href="<?php echo base_url() ?>" style="color:#fff;text-decoration:none;"> <?php echo systems('site_footer') ?></a>, All rights reserved.</p>
  <!-- <p>Developed by <a href="https://www.bismalabs.co.id" target="_blank" style="color:#fff;text-decoration:none">BismaLabs.co.id</a></p> -->
</div>
<script src="<?php echo base_url() ?>theme/js/jQuery.js"></script>
<script src="<?php echo base_url() ?>theme/js/bootstrap.min.js"></script>
<script src="<?php echo base_url() ?>theme/js/iziToast.js"></script>
<script src="<?php echo base_url() ?>theme/js/breakingNews.js"></script>
<script src="<?php echo base_url() ?>theme/js/app.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/lightbox2/2.10.0/js/lightbox.js"></script>
<?php if (isset($data_js)) {
  echo $data_js;
} ?>
<script>
  <?php if (isset($js_ready)) {
    echo $js_ready;
  } ?>
</script>
<script>
  $('form').submit(function() {
    var kps = $("input#ya_kps:checked").val();
    var nomor_kps = $("input#no_kps").val();

    if (kps == "Ya" && nomor_kps == "") {
      alert("Nomor KPS Wajib Diisi");
      $("#nav-home").addClass('active');
      $("input[name=no_kps]").focus();
    }
  });
</script>
<script src="<?php echo base_url() . 'theme/js/jssocials.js' ?>"></script>
<script>
  $(document).ready(function() {
    $(".sharePopup").jsSocials({
      showCount: true,
      showLabel: true,
      shareIn: "popup",
      shares: [{
          share: "twitter",
          label: "Twitter"
        },
        {
          share: "facebook",
          label: "Facebook"
        },
        {
          share: "whatsapp",
          label: "Whatsapp"
        }
      ]
    });
  });

  $(document).ready(function() {
    $(window).scroll(function(event) {
      var scroll = $(window).scrollTop();
      if (scroll > 5) {
        $('.navbar').addClass('shadow');
      }else{
        $('.navbar').removeClass('shadow');
      }
    });
  })
</script>

</body>

</html>