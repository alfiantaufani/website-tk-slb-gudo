<div class="container">
    <div class="breakingNews" id="bn5" style="margin-bottom:10px;box-shadow: rgba(159, 167, 194, 0.6) 0px 1px 1px 0px;">
        <div class="bn-title" style="background:<?php echo theme_color_body() ?>">
            <h2><i class="fa fa-newspaper-o"></i>
                KABAR TERBARU</h2><span style="border-left-color:<?php echo theme_color_body() ?>"></span>
        </div>
        <ul>
            <?php
            if (news_ticker() != NULL) :
                foreach (news_ticker() as $hasil) :
            ?>

                    <li>
                        <a href="<?php echo base_url() ?>artikel/<?php echo $hasil->tulisan_slug ?>/"><?php echo $hasil->tulisan_judul ?></a>
                    </li>

                <?php endforeach; ?>

            <?php elseif (news_ticker() == NULL) : ?>

                <li><a href="#">Tidak ada berita terbaru!</a></li>

            <?php endif; ?>

        </ul>
        <div class="bn-navi">
            <span></span>
            <span></span>
        </div>

    </div>
</div>

<div class="container">

    <div class="row">

        <div class="col-md-12">
            <!-- slider -->
            <div id="slider" class="carousel slide shadow border-radius-10" data-ride="carousel">
                <div class="carousel-inner" role="listbox">

                    <?php
                    $item_class = ' active';
                    $data_slider = $this->db->query("SELECT * FROM tbl_slider ORDER BY id ASC");
                    foreach ($data_slider->result() as $hasil) :
                        //$item_class = ($i == 1) ? 'item active' : 'item';
                    ?>

                        <div class="carousel-item <?php echo $item_class; ?>" style="width:100%;">
                            <img class="d-block img-fluid" src="<?php echo base_url() ?>assets/images/slider/<?php echo $hasil->image ?>" alt="<?php echo $hasil->caption ?>">
                            <div class="carousel-caption d-none d-md-block">
                                <h3><?php echo $hasil->caption ?></h3>
                                <p><?= $hasil->description ?></p>
                                <?php if ($hasil->url == '#' || $hasil->url == "") : ?>

                                <?php else : ?>
                                    <a href="<?= $hasil->url ?>" class="btn btn-outline-light btn-lg"> Selengkapnya</a>
                                <?php endif ?>
                            </div>
                        </div>

                    <?php
                        $item_class = '';
                    endforeach;
                    ?>

                </div>
                <a class="carousel-control-prev" href="#slider" role="button" data-slide="prev">
                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="carousel-control-next" href="#slider" role="button" data-slide="next">
                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                </a>
            </div>
            <!-- end slider -->
        </div>
    </div>


    <div class="row">
        <!-- content kiri -->
        <div class="col-xs-12 col-md-9">

            <!-- berita baru -->
            <div class="col-xs-12 col-md-12 mb-2">
                <div class="row">
                    <h5 class="font-weight-bold mt-5">BERITA TERBARU</h5>
                    <div class="ml-auto mt-4 pt-4">
                        <a href="<?= base_url('artikel') ?>" class="text-decoration-none">Lihat Semua <i class="fa fa-angle-right"></i></a>
                    </div>
                </div>
            </div>
            <div class="row">
                <?php
                if ($berita != NULL) :
                    foreach ($berita->result() as $hasil) :

                        //limit judul
                        if (strlen($hasil->tulisan_judul) < 40) {
                            $judul = $hasil->tulisan_judul;
                        } else {
                            $judul = substr($hasil->tulisan_judul, 0, 40) . '...';
                        }

                        //limit tulisan_deskripsi
                        if (strlen($hasil->tulisan_deskripsi) < 55) {
                            $tulisan_deskripsi = $hasil->tulisan_deskripsi;
                        } else {
                            $tulisan_deskripsi = substr($hasil->tulisan_deskripsi, 0, 55) . '...';
                        }

                ?>
                        <div class="col-md-6">
                            <div class="card border-radius-10 shadow mb-3">
                                <img class="card-img-top" src="<?php echo base_url() ?>assets/images/<?php echo $hasil->tulisan_gambar ?>" alt="Card image cap">
                                <div class="card-body">
                                    <p class="card-text">
                                        <small class="text-muted">
                                            <i class="fa fa-calendar-o"></i> <?php echo $this->web->tgl_indo_no_hari($hasil->tulisan_tanggal) ?>
                                        </small>
                                    </p>
                                    <a href="<?php echo base_url() ?>artikel/<?php echo $hasil->tulisan_slug ?>/" style="text-decoration:none;color:<?php theme_color_body() ?>">
                                        <h5 class="card-title"><b> <?php echo $judul ?> </b></h5>
                                    </a>
                                    <p class="card-text"><?php echo $tulisan_deskripsi ?></p>
                                </div>
                            </div>
                        </div>

                    <?php
                    endforeach;
                else :
                    ?>
                    Tidak Berita Disini
                <?php endif; ?>

            </div>
            <!-- end berita baru -->

            <!-- agenda baru -->
            <div class="col-xs-12 col-md-12 mb-2">
                <div class="row">
                    <h5 class="font-weight-bold mt-5">AGENDA TERBARU</h5>
                    <div class="ml-auto mt-4 pt-4">
                        <a href="<?= base_url('agenda') ?>" class="text-decoration-none">Lihat Semua <i class="fa fa-angle-right"></i></a>
                    </div>
                </div>
            </div>
            <div class="row">
                <?php
                if ($agenda != NULL) :
                    foreach ($agenda->result() as $hasil) :

                        //limit judul
                        if (strlen($hasil->agenda_nama) < 40) {
                            $judul = $hasil->agenda_nama;
                        } else {
                            $tanggal = $hasil->agenda_mulai;
                            $judul = substr($hasil->agenda_nama, 0, 40) . '...';
                        }

                        //limit tulisan_deskripsi
                        if (strlen($hasil->agenda_deskripsi) < 55) {
                            $agenda_deskripsi = $hasil->agenda_deskripsi;
                        } else {
                            $agenda_deskripsi = substr($hasil->agenda_deskripsi, 0, 55) . '...';
                        }

                ?>
                        <div class="col-md-6">
                            <div class="card border-radius-10 shadow border-0">
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-xl-12">
                                            <div class="widget-49">
                                                <div class="widget-49-title-wrapper">
                                                    <div class="widget-49-date-primary">
                                                        <span class="widget-49-date-day"><?php echo date("d", strtotime($hasil->agenda_mulai)); ?></span>
                                                        <span class="widget-49-date-month"><?php echo date("M", strtotime($hasil->agenda_mulai)); ?></span>
                                                    </div>
                                                    <div class="widget-49-meeting-info">
                                                        <span class="widget-49-pro-title">
                                                            <a href="<?php echo base_url() ?>agenda/<?= $hasil->agenda_id ?>" style="text-decoration:none;color:<?php theme_color_body() ?>">
                                                                <?php echo $judul ?>
                                                            </a>
                                                        </span>
                                                        <span class="widget-49-meeting-time">
                                                            <i class="fa fa-calendar-o"></i>
                                                            <?php echo $this->web->tgl_indo_no_hari($hasil->agenda_tanggal) ?>
                                                        </span>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    <?php
                    endforeach;
                else :
                    ?>
                    Tidak Agenda Disini
                <?php endif; ?>

            </div>
            <!-- end agenda baru -->

            <!-- pengumuman baru -->
            <div class="col-xs-12 col-md-12 mb-2">
                <div class="row">
                    <h5 class="font-weight-bold mt-5">PENGUMUMAN TERBARU</h5>
                    <div class="ml-auto mt-4 pt-4">
                        <a href="<?= base_url('pengumuman') ?>" class="text-decoration-none">Lihat Semua <i class="fa fa-angle-right"></i></a>
                    </div>
                </div>
            </div>
            <div class="row">
                <?php
                if ($pengumuman != NULL) :
                    foreach ($pengumuman->result() as $hasil) :

                        //limit judul
                        if (strlen($hasil->pengumuman_judul) < 40) {
                            $judul = $hasil->pengumuman_judul;
                        } else {
                            $judul = substr($hasil->pengumuman_judul, 0, 40) . '...';
                        }
                        $tgl = $hasil->tanggal;

                        //limit tulisan_deskripsi
                        if (strlen($hasil->pengumuman_deskripsi) < 55) {
                            $pengumuman_deskripsi = $hasil->pengumuman_deskripsi;
                        } else {
                            $pengumuman_deskripsi = substr($hasil->pengumuman_deskripsi, 0, 55) . '...';
                        }

                ?>
                        <div class="col-md-6 mb-4">
                            <div class="job-card border-radius-10 shadow">
                                <div class="job-card__content">
                                    <div class="job-card_img">
                                        <i class="fa fa-bell-o fa-2x p-4 rounded bg-theme-circle text-primary rounded-circle"></i>
                                    </div>
                                    <div class="job-card_info">
                                        <h6>
                                            <a href="<?php echo base_url() ?>pengumuman/<?= $hasil->pengumuman_id ?>" style="text-decoration:none;color:<?php theme_color_body() ?>">
                                                <?php echo $judul ?>
                                            </a>
                                        </h6>

                                        <small class="text-muted"><i class="fa fa-calendar-o"></i> <?php echo $this->web->tgl_indo_no_hari($tgl) ?></small><br>
                                        <small class="card-text"><?php echo $pengumuman_deskripsi ?></small>
                                    </div>
                                </div>
                                <div class="job-card__footer">
                                </div>
                            </div>
                        </div>

                    <?php
                    endforeach;
                else :
                    ?>
                    Tidak Pengumuman Disini
                <?php endif; ?>
            </div>
            <!-- end pengumuman baru -->



        </div>
        <!-- end content kiri -->

        <!-- content kanan -->
        <div class="col-xs-12 col-md-3">
            <div class="col-xs-12 col-md-12 mb-2">
                <div class="row">
                    <h5 class="font-weight-bold mt-5">KATEGORI BERITA</h5>
                </div>
            </div>
            <ul class="list-group">
                <?php
                if (kategori() != NULL) :
                    foreach (kategori() as $hasil) :

                        $query = $this->db->query("SELECT count(tulisan_kategori_id) as jumlah FROM tbl_tulisan WHERE tulisan_kategori_id ='$hasil->kategori_id'")->row();

                ?>

                        <li class="list-group-item d-flex justify-content-between align-items-center">
                            <a href="<?php echo site_url('blog/kategori/' . str_replace(" ", "-", $hasil->kategori_nama)); ?>" style="text-decoration:none;color:#333;text-transform:uppercase"><i class="fa fa-folder"></i> <?php echo $hasil->kategori_nama ?></a>
                            <span class="badge badge-success badge-pill" style="background-color: <?php echo theme_color_body(); ?>;padding: 6px"><?php echo $query->jumlah ?></span>
                        </li>

                    <?php endforeach; ?>

                <?php elseif (news_ticker() == NULL) : ?>

                    <li><a href="#">Tidak ada data kategori!</a></li>

                <?php endif; ?>
            </ul>
        </div>
        <!-- end content kanan -->
    </div>

</div>