<div class="container">
    <div class="row">
        <div class="col-xs-12 col-md-12">
            <div class="text-center mt-4 mb-5">
                <h2 class="font-weight-bold ">Pengumuman</h2>
                <p class="text-muted">Berikut Halaman Pengumuman <?php echo systems('site_title') ?></p>
            </div>
        </div>
        <!-- content kiri -->
        <div class="col-xs-12 col-md-9 mt-2">
            <div class="row">
                <?php
                if ($data != NULL) :
                    foreach ($data->result() as $hasil) :

                        //limit judul
                        if (strlen($hasil->pengumuman_judul) < 50) {
                            $judul = $hasil->pengumuman_judul;
                        } else {
                            $judul = substr($hasil->pengumuman_judul, 0, 50) . '...';
                        }
                        $tgl = $hasil->tanggal;

                        //limit tulisan_deskripsi
                        if (strlen($hasil->pengumuman_deskripsi) < 55) {
                            $pengumuman_deskripsi = $hasil->pengumuman_deskripsi;
                        } else {
                            $pengumuman_deskripsi = substr($hasil->pengumuman_deskripsi, 0, 55) . '...';
                        }

                ?>
                        <div class="col-md-6 mb-4">
                            <div class="job-card shadow">
                                <div class="job-card__content">
                                    <div class="job-card_img">
                                        <i class="fa fa-bell-o fa-2x p-4 rounded bg-theme-circle text-primary rounded-circle"></i>
                                    </div>
                                    <div class="job-card_info">
                                        <h6>
                                            <a href="<?php echo base_url() ?>pengumuman/<?= $hasil->pengumuman_id ?>" style="text-decoration:none;color:<?php theme_color_body() ?>">
                                                <?php echo $judul ?>
                                            </a>
                                        </h6>

                                        <small class="text-muted"><i class="fa fa-calendar-o"></i> <?php echo $this->web->tgl_indo_no_hari($tgl) ?></small><br>
                                        <small class="card-text"><?php echo $pengumuman_deskripsi ?></small>
                                    </div>
                                </div>
                                <div class="job-card__footer">
                                </div>
                            </div>
                        </div>

                    <?php
                    endforeach;
                else :
                    ?>
                    Tidak Pengumuman Disini
                <?php endif; ?>
                <div class="col-md-12 text-center mt-5">
                    <?php echo $page; ?>
                </div>
            </div>
        </div>

        <!-- content kanan -->
        <div class="col-xs-12 col-md-3">
            <div class="col-xs-12 col-md-12 mb-2">
                <div class="row">
                    <h5 class="font-weight-bold mt-1">KATEGORI BERITA</h5>
                </div>
            </div>
            <ul class="list-group">
                <?php
                if (kategori() != NULL) :
                    foreach (kategori() as $hasil) :

                        $query = $this->db->query("SELECT count(tulisan_kategori_id) as jumlah FROM tbl_tulisan WHERE tulisan_kategori_id ='$hasil->kategori_id'")->row();

                ?>

                        <li class="list-group-item d-flex justify-content-between align-items-center">
                            <a href="<?php echo site_url('blog/kategori/' . str_replace(" ", "-", $hasil->kategori_nama)); ?>" style="text-decoration:none;color:#333;text-transform:uppercase"><i class="fa fa-folder"></i> <?php echo $hasil->kategori_nama ?></a>
                            <span class="badge badge-success badge-pill" style="background-color: <?php echo theme_color_body(); ?>;padding: 6px"><?php echo $query->jumlah ?></span>
                        </li>

                    <?php endforeach; ?>

                <?php elseif (news_ticker() == NULL) : ?>

                    <li><a href="#">Tidak ada data kategori!</a></li>

                <?php endif; ?>
            </ul>
        </div>
        <!-- end content kanan -->
    </div>
</div>