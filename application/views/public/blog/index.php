<div class="container">
    <div class="row">
        <div class="col-xs-12 col-md-12">
            <div class="text-center mt-4 mb-5">
                <h2 class="font-weight-bold ">Artikel</h2>
                <p class="text-muted">Berikut Halaman Artikel <?php echo systems('site_title') ?></p>
            </div>
        </div>

        <!-- content kiri -->
        <div class="col-xs-12 col-md-9 mt-3">

            <!-- berita baru -->

            <div class="row">

                <?php
                if ($data != NULL) :
                    foreach ($data->result() as $hasil) :

                        //limit judul
                        if (strlen($hasil->tulisan_judul) < 40) {
                            $judul = $hasil->tulisan_judul;
                        } else {
                            $judul = substr($hasil->tulisan_judul, 0, 40) . '...';
                        }

                        //limit tulisan_deskripsi
                        if (strlen($hasil->tulisan_deskripsi) < 55) {
                            $tulisan_deskripsi = $hasil->tulisan_deskripsi;
                        } else {
                            $tulisan_deskripsi = substr($hasil->tulisan_deskripsi, 0, 55) . '...';
                        }

                ?>
                        <div class="col-md-6">
                            <div class="card border-radius-10 shadow mb-3">
                                <img class="card-img-top" src="<?php echo base_url() ?>assets/images/<?php echo $hasil->tulisan_gambar ?>" alt="Card image cap">
                                <div class="card-body">
                                    <p class="card-text">
                                        <small class="text-muted">
                                            <i class="fa fa-calendar-o"></i> <?php echo $this->web->tgl_indo_no_hari($hasil->tulisan_tanggal) ?>
                                        </small>
                                    </p>
                                    <a href="<?php echo base_url() ?>artikel/<?php echo $hasil->tulisan_slug ?>/" style="text-decoration:none;color:<?php theme_color_body() ?>">
                                        <h5 class="card-title"><b> <?php echo $judul ?> </b></h5>
                                    </a>
                                    <p class="card-text"><?php echo $tulisan_deskripsi ?></p>
                                </div>
                            </div>
                        </div>

                    <?php
                    endforeach;
                else :
                    ?>
                    Tidak Berita Disini
                <?php endif; ?>
                <div class="col-md-12 text-center mt-4">
                    <?php
                    error_reporting(0);
                    echo $page;
                    ?>
                </div>
            </div>

        </div>
        <!-- end content kiri -->

        <!-- content kanan -->
        <div class="col-xs-12 col-md-3">
            <h5 class="font-weight-bold mt-4">KATEGORI BERITA</h5>
            <ul class="list-group">
                <?php
                if (kategori() != NULL) :
                    foreach (kategori() as $hasil) :

                        $query = $this->db->query("SELECT count(tulisan_kategori_id) as jumlah FROM tbl_tulisan WHERE tulisan_kategori_id ='$hasil->kategori_id'")->row();

                ?>

                        <li class="list-group-item d-flex justify-content-between align-items-center">
                            <a href="<?php echo site_url('blog/kategori/' . str_replace(" ", "-", $hasil->kategori_nama)); ?>" style="text-decoration:none;color:#333;text-transform:uppercase"><i class="fa fa-folder"></i> <?php echo $hasil->kategori_nama ?></a>
                            <span class="badge badge-success badge-pill" style="background-color: <?php echo theme_color_body(); ?>;padding: 6px"><?php echo $query->jumlah ?></span>
                        </li>

                    <?php endforeach; ?>

                <?php elseif (news_ticker() == NULL) : ?>

                    <li><a href="#">Tidak ada data kategori!</a></li>

                <?php endif; ?>
            </ul>


            <!-- end content kanan -->
        </div>
    </div>
</div>