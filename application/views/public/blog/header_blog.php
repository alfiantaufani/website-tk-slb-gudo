<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="theme-color" content="<?php theme_color_header() ?>">
    <meta name="mobile-web-app-capable" content="yes">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <meta name="author" content="<?php echo systems('site_title'); ?>">
    <meta name="robots" content="no-cache">
    <meta name="description" content="<?php echo strip_tags(systems('description')); ?>">

    <meta name="keywords" content="<?php echo $kategori ?>, <?php echo url_title($title, ', ', TRUE)?>">
    <meta property="og:url" content="<?php echo base_url($this->uri->uri_string()); ?>">
    <meta property="og:site_name" content="<?php echo systems('site_title'); ?>">
    <meta property="og:title" content="<?php echo $title; ?>">
    <meta property="og:description" content="<?php echo $deskripsi; ?>">
    <meta property="og:image" content="<?php echo base_url() . 'assets/images/' . $image ?>">
    <meta property="twitter:site" content="@bismalabs">
    <meta property="twitter:site:id" content="75438343">
    <meta property="twitter:card" content="summary">
    <meta property="twitter:title" content="<?php echo systems('site_title'); ?>">
    <meta property="twitter:description" content="<?php echo $deskripsi; ?>">
    <meta property="twitter:image:src" content="<?php echo base_url() . 'assets/images/' . $image ?>">
    <meta name="csrf-param" content="<?php echo $this->security->get_csrf_token_name(); ?>" />
    <meta name="csrf-token" content="<?php echo base64_encode($this->security->get_csrf_hash()); ?>" />
    <meta name="google-site-verification" content=" " />
    <title> <?php echo systems('site_title') ?> | <?php echo $title; ?></title>
    <link href="<?php echo base_url() ?>theme/css/style.css" rel="stylesheet">
    <link href="<?php echo base_url() ?>theme/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo base_url() ?>theme/css/iziToast.css" rel="stylesheet">
    <link href="<?php echo base_url() ?>theme/css/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <!--Social Share-->
    <link href="<?php echo base_url() . 'theme/css/jssocials.css' ?>" rel="stylesheet">
    <link href="<?php echo base_url() . 'theme/css/jssocials-theme-flat.css' ?>" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/lightbox2/2.10.0/css/lightbox.min.css">
    <link rel="icon" href="<?php echo base_url() ?>theme/images/logo/<?= systems('logo') ?>">

    <style>
        .btn-success {
            color: #fff;
            background-color: <?php theme_color_body() ?>;
            border-color: <?php theme_color_body() ?>;
        }

        .btn-success:hover {
            color: #fff;
            background-color: <?php theme_color_header() ?>;
            border-color: <?php theme_color_header() ?>;
        }
    </style>
    <script>
        var BASE_URL = "<?php echo base_url() ?>";
    </script>

</head>

<body style="background-color:#e4e9f3d6;">

    <nav class="navbar navbar-expand-md navbar-dark fixed-top bg-dark" style="background-color:<?php echo theme_color_navbar(); ?>!important;border-bottom:<?php echo theme_color_border_navbar() ?> solid 2px;font-weight:400;font-size:14px;color:white">
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExampleDefault" aria-controls="navbarsExampleDefault" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarsExampleDefault">
            <div class="container">
                <div class="row">
                    <ul class="navbar-nav mr-auto" style="margin-top:2px">

                        <?php
                        $menu = $this->db->get('tbl_menu')->result();
                        foreach ($menu as $menus) {
                            $id_menu = $menus->id;
                            $cek_submenu = $this->db->query("SELECT * FROM tbl_submenu WHERE menu_id='$id_menu'");
                            if ($cek_submenu->num_rows() > 0) {
                                echo '
                                <li class="nav-item dropdown" style="margin-left:5px;">
                                    <a class="nav-link dropdown-toggle" style="color:white" href="#" id="dropdown02" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    ' . $menus->menu_name . '
                                    </a>
                                    <div class="dropdown-menu" aria-labelledby="dropdown01" style="font-size:14px">';
                                $submenu = $this->db->query("SELECT * FROM tbl_submenu WHERE menu_id='$id_menu'")->result_array();
                                foreach ($submenu as $sub) {
                                    echo '<a class="dropdown-item" style="text-transform:uppercase" href="' . base_url() . '' . $sub['submenu_link'] . '"> <i class="fa fa-check-circle"></i> ' . $sub['submenu_name'] . ' </a>';
                                }
                                echo '    
                                    </div>
                                </li>
                                ';
                            } else {
                                echo '
                                <li class="nav-item active">
                                    <a class="nav-link" href="' . base_url($menus->menu_link) . '" style="color:white">
                                        ' . $menus->menu_name . '
                                    </a>
                                </li>
                                ';
                            }
                        }
                        ?>




                    </ul>
                    <div class="search">
                        <form class="form-inline search-navbar navbar-form navbar-right hidden-sm hidden-md hidden-xs" role="search" method="GET" action="<?php echo site_url('blog/search'); ?>">
                            <input class="form-control mr-sm-2" type="search" name="keyword" placeholder="Cari sesuatu disini..." aria-label="Search" required>
                            <button class="btn btn-dark my-2 my-sm-0" style="cursor: pointer" type="submit"><i class="fa fa-search"></i> Cari</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </nav>

    <!-- Main jumbotron for a primary marketing message or call to action -->
    <div class="jumbotron" style="background-color: <?php echo theme_color_header(); ?>;padding-bottom:0px;border-radius:0px;margin-bottom:10px;border-bottom: 5px solid <?php echo theme_color_navbar(); ?>">
        <div class="container">

            <div class="row">

                <div class="col-md-8">
                    <div class="header-logo">
                        <a href="<?php echo base_url() ?>"><img src="<?php echo base_url() ?>theme/images/logo/<?php echo systems('logo') ?>" alt="" class="img-responsive" style="width: 150px"></a>
                    </div>
                    <div class="header-text mb-5">
                        <h2 style="color: #fff; font-weight: bold;padding-top:25px"><?php echo systems('site_title') ?></h2>
                        <p style="color:#fff;font-weight:500"><?php echo systems('description') ?></p>
                        <p style="color:#fff;padding-top:5px;font-weight:500"><?php echo systems('address') ?></p>
                    </div>
                </div>

                <div class="col-md-4">
                    <div class="social-icon" style="margin-top: 50px;text-align: center">
                        <a class="social" href="<?php echo systems('facebook') ?>" target="_blank" style="margin-right: .5rem!important;">
                            <img src="<?php echo base_url() ?>theme/images/social/facebook.png" width="50" height="50" alt="Facebook" />
                        </a>
                        <a class="social" href="<?php echo systems('twitter') ?>" target="_blank" style="margin-right: .5rem!important;">
                            <img src="<?php echo base_url() ?>theme/images/social/twitter.png" width="50" height="50" alt="Twitter" />
                        </a>
                        <a class="social" href="<?php echo systems('instagram') ?>" target="_blank">
                            <img src="<?php echo base_url() ?>theme/images/social/instagram-color.png" width="50" height="50" alt="Google+" />
                        </a>
                    </div>
                </div>

            </div>

        </div>
    </div>
    </div>