<div class="container">
    <div class="row">
        <!-- content kiri -->
        <div class="col-xs-12 col-md-9 mt-3">

            <!-- berita baru -->

            <div class="row">
                <div class="card" style="width: 100%;">
                    <div class="card-body">
                        <div class="judul" style="font-size:30px;font-weight:500"><b><?php echo $title; ?></b></div>
                        <hr>
                        <div class="d-flex justify-content-between align-items-center">
                            <small style="font-size: 14.5px;">
                                <a href="javascript:void(0)" class="text-decoration-none">
                                    <i class="fa fa-user-circle-o"></i> <?php echo $author; ?>
                                </a>
                                |
                                <i class="fa fa-calendar-o"></i>
                                <?php echo $this->web->tgl_indo_no_hari($tanggal) ?>
                                |
                                <i class="fa fa-tags"></i>
                                <?php echo $kategori ?>
                            </small>
                            <div class="sharePopup"></div>
                        </div>
                        <hr>
                        <img src="<?php echo base_url() . 'assets/images/' . $image ?>" class="img-fluid rounded mx-auto d-block" alt="blog-img">
                        <br>
                        <p><?php echo $blog; ?></p>
                        <hr>

                        <ul class="nav nav-tabs blogpost-tab-wrap" role="tablist">
                            <li class="nav-item blogpost-nav-tab">
                                <a class="nav-link active" data-toggle="tab" href="#comments" role="tab">Komentar</a>
                            </li>
                            <li class="nav-item blogpost-nav-tab">
                                <a class="nav-link" data-toggle="tab" href="#write-comment" role="tab">Tinggalkan Komentar</a>
                            </li>
                        </ul>
                        <div class="clearfix"></div>
                        <?php echo $this->session->flashdata('msg'); ?>
                        <div class="blogpost-tabs">
                            <!-- Tab panes -->
                            <div class="tab-content">
                                <div class="tab-pane active mt-3" id="comments" role="tabpanel">
                                    <?php
                                    $colors = array(
                                        '#ff9e67',
                                        '#10bdff',
                                        '#14b5c7',
                                        '#f98182',
                                        '#8f9ce2',
                                        '#ee2b33',
                                        '#d4ec15',
                                        '#613021',
                                    );
                                    foreach ($show_komentar->result() as $row) :
                                        shuffle($colors);
                                    ?>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="row">
                                                    <div class="col-md-2">
                                                        <div class="blodpost-tab-img" style="background-color:<?php echo reset($colors); ?>;width: 65px;height: 65px;border-radius:50px 50px 50px 50px;">
                                                            <center>
                                                                <h2 style="padding-top:20%;color:#fff;"><?php echo substr($row->komentar_nama, 0, 1); ?></h2>
                                                            </center>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-10">
                                                        <div class="blogpost-tab-description">
                                                            <h6><?php echo $row->komentar_nama; ?></h6><small><em class="text-muted"><?php echo $this->web->tgl_indo_no_hari($row->komentar_tanggal); ?></em></small>
                                                            <p><?php echo $row->komentar_isi; ?></p>
                                                        </div>
                                                        <hr>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <?php
                                        $komentar_id = $row->komentar_id;
                                        $query = $this->db->query("SELECT * FROM tbl_komentar WHERE komentar_status='1' AND komentar_parent='$komentar_id' ORDER BY komentar_id ASC");
                                        foreach ($query->result() as $res) :
                                            shuffle($colors);
                                        ?>
                                            <div class="row">
                                                <div class="col-md-12 offset-md-1">
                                                    <div class="row">
                                                        <div class="col-md-2">
                                                            <div class="blodpost-tab-img" style="background-color:<?php echo reset($colors); ?>;width: 65px;height: 65px;border-radius:50px 50px 50px 50px;">
                                                                <center>
                                                                    <h2 style="padding-top:20%;color:#fff;"><?php echo substr($res->komentar_nama, 0, 1); ?></h2>
                                                                </center>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-9">
                                                            <div class="blogpost-tab-description">
                                                                <h6>
                                                                    <?php echo $res->komentar_nama; ?>
                                                                </h6>
                                                                <small>
                                                                    <em class="text-muted">
                                                                        <?php echo $this->web->tgl_indo_no_hari($res->komentar_tanggal); ?>
                                                                    </em>
                                                                </small>
                                                                <p><?php echo $res->komentar_isi; ?></p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        <?php endforeach; ?>
                                    <?php endforeach; ?>
                                </div>
                                <div class="tab-pane mt-3" id="write-comment" role="tabpanel">
                                    <form action="<?php echo site_url('blog/komentar'); ?>" method="post">
                                        <div class="row">
                                            <div class="col-6">
                                                <div class="form-group">
                                                    <label>Nama</label>
                                                    <input type="text" class="form-control" name="nama" placeholder="Nama Lengkap" required>
                                                </div>
                                                <!-- // end .form-group -->
                                            </div>
                                            <div class="col-6">
                                                <div class="form-group">
                                                    <label>Email</label>
                                                    <input type="email" class="form-control" name="email" placeholder="Email" required>
                                                </div>
                                                <!-- // end .form-group -->
                                            </div>
                                            <div class="col-12">
                                                <div class="form-group">
                                                    <label>Komentar Anda</label>
                                                    <textarea class="form-control" name="komentar" placeholder="Masukkan komentar" rows="6" required> </textarea>
                                                </div>
                                                <!-- // end .form-group -->
                                            </div>
                                            <div class="col-12">
                                                <input type="hidden" name="id" value="<?php echo $id; ?>" required>
                                                <button type="submit" class="btn btn-warning">Kirim Komentar</button>
                                            </div>
                                            <!-- // end .col-12 -->
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- end content kiri -->

        <!-- content kanan -->
        <div class="col-xs-12 col-md-3">
            <h5 class="font-weight-bold mt-4">KATEGORI BERITA</h5>
            <ul class="list-group">
                <?php
                if (kategori() != NULL) :
                    foreach (kategori() as $hasil) :

                        $query = $this->db->query("SELECT count(tulisan_kategori_id) as jumlah FROM tbl_tulisan WHERE tulisan_kategori_id ='$hasil->kategori_id'")->row();

                ?>

                        <li class="list-group-item d-flex justify-content-between align-items-center">
                            <a href="<?php echo site_url('blog/kategori/' . str_replace(" ", "-", $hasil->kategori_nama)); ?>" style="text-decoration:none;color:#333;text-transform:uppercase"><i class="fa fa-folder"></i> <?php echo $hasil->kategori_nama ?></a>
                            <span class="badge badge-success badge-pill" style="background-color: <?php echo theme_color_body(); ?>;padding: 6px"><?php echo $query->jumlah ?></span>
                        </li>

                    <?php endforeach; ?>

                <?php elseif (news_ticker() == NULL) : ?>

                    <li><a href="#">Tidak ada data kategori!</a></li>

                <?php endif; ?>
            </ul>


            <!-- end content kanan -->
        </div>
    </div>
</div>