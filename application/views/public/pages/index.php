<div class="container">
    <div class="row">
        <!-- content kiri -->
        <?php if ($this->uri->segment(1) == 'ppdb') : ?>
            <div class="col-xs-12 col-md-3 mt-3">
                <div class="card shadow">
                    <div class="card-body">
                        <label><i class="fa fa-home"></i> MAIN MENU</label>
                        <ul class="list-group list-group-flush">
                            <li class="list-group-item"><a href="<?= base_url() ?>ppdb/" class="text-decoration-none">PPDB</a> </li>
                            <?php
                            if ($menu != NULL) :
                                foreach ($menu as $hasil) :
                            ?>
                                    <li class="list-group-item"><a href="<?= base_url() ?>ppdb/<?= $hasil->menu_link ?>" class="text-decoration-none"><?= $hasil->menu_name ?></a> </li>
                                <?php
                                endforeach;
                            else :
                                ?>
                                Menu Belum Tersedia
                            <?php endif; ?>
                        </ul>
                    </div>
                </div>
            </div>
        <?php endif ?>

        <div class="col-xs-12 col-md-9 mt-3">
            <div class="row">
                <div class="card" style="width:100%">
                    <div class="card-body">

                        <?php foreach ($pages->result_array() as $i) : ?>
                            <div class="judul" style="font-size:30px;font-weight:500"><?php echo $title ?></div>
                            <hr>
                            <div class="d-flex justify-content-between align-items-center">
                                <h5>
                                    <a href="javascript:void(0)" class="text-decoration-none">
                                        <i class="fa fa-user-circle-o"></i> <?php echo $i['pengguna_nama'] ?>
                                    </a>
                                </h5>
                                <div class="sharePopup"></div>
                            </div>
                            <hr>

                            <?php echo $i['content'] ?>
                            <hr>


                        <?php endforeach; ?>

                    </div>
                </div>


            </div>

        </div>
        <!-- end content kiri -->

        <!-- content kanan -->
        <?php if ($this->uri->segment(1) != 'ppdb') : ?>
            <div class="col-xs-12 col-md-3">
                <h5 class="font-weight-bold mt-4">KATEGORI BERITA</h5>
                <ul class="list-group">
                    <?php
                    if (kategori() != NULL) :
                        foreach (kategori() as $hasil) :

                            $query = $this->db->query("SELECT count(tulisan_kategori_id) as jumlah FROM tbl_tulisan WHERE tulisan_kategori_id ='$hasil->kategori_id'")->row();

                    ?>

                            <li class="list-group-item d-flex justify-content-between align-items-center">
                                <a href="<?php echo site_url('blog/kategori/' . str_replace(" ", "-", $hasil->kategori_nama)); ?>" style="text-decoration:none;color:#333;text-transform:uppercase"><i class="fa fa-folder"></i> <?php echo $hasil->kategori_nama ?></a>
                                <span class="badge badge-success badge-pill" style="background-color: <?php echo theme_color_body(); ?>;padding: 6px"><?php echo $query->jumlah ?></span>
                            </li>

                        <?php endforeach; ?>

                    <?php elseif (news_ticker() == NULL) : ?>

                        <li><a href="#">Tidak ada data kategori!</a></li>

                    <?php endif; ?>
                </ul>


                <!-- end content kanan -->
            </div>
        <?php endif ?>
    </div>
</div>