<div class="container">
    <div class="row">
        <!-- content kiri -->
        <div class="col-xs-12 col-md-9 mt-3">

            <!-- berita baru -->

            <div class="row">
                <div class="card" style="width: 100%;">
                    <div class="card-body">
                        <div class="judul" style="font-size:30px;font-weight:500"><?php echo $data->agenda_nama ?></div>
                        <hr>
                        <div class="d-flex justify-content-between align-items-center">
                            <h5>
                                <a href="javascript:void(0)" class="text-decoration-none">
                                    <i class="fa fa-user-circle-o"></i> <?php echo $data->agenda_author ?>
                                </a>
                                |
                                <i class="fa fa-calendar-o"></i>
                                <?php echo $this->web->tgl_indo_no_hari($data->agenda_tanggal) ?>
                            </h5>
                            <div class="sharePopup"></div>
                        </div>
                        <hr>
                        <?php if ($data->image != NULL) : ?>
                            <img src="<?php echo base_url() . 'assets/images/' . $data->image ?>" class="img-fluid rounded mx-auto d-block" alt="blog-img">
                        <?php endif ?>
                        <br>
                        <div class="table-responsive">
                            <table class="table table-striped">
                                <tr>
                                    <th class="w-auto mw-100">Agenda Mulai</th>
                                    <td><?= $data->agenda_mulai ?></td>
                                </tr>
                                <tr>
                                    <th class="w-auto mw-100">Agenda Selesai</th>
                                    <td><?= $data->agenda_selesai ?></td>
                                </tr>
                                <tr>
                                    <th class="w-auto mw-100">Agenda Tempat</th>
                                    <td><?= $data->agenda_tempat ?></td>
                                </tr>
                                <tr>
                                    <th class="w-auto mw-100">Agenda Waktu</th>
                                    <td><?= $data->agenda_waktu ?></td>
                                </tr>
                                <tr>
                                    <th class="w-auto mw-100">Agenda Keterangan</th>
                                    <td><?= $data->agenda_keterangan ?></td>
                                </tr>
                                <tr>
                                    <th class="w-auto mw-100">Agenda Deskripsi</th>
                                    <td><?= $data->agenda_deskripsi ?></td>
                                </tr>
                            </table>
                        </div>
                        <hr>
                    </div>
                </div>
            </div>
        </div>
        <!-- end content kiri -->

        <!-- content kanan -->
        <div class="col-xs-12 col-md-3">
            <h5 class="font-weight-bold mt-4">KATEGORI BERITA</h5>
            <ul class="list-group">
                <?php
                if (kategori() != NULL) :
                    foreach (kategori() as $hasil) :

                        $query = $this->db->query("SELECT count(tulisan_kategori_id) as jumlah FROM tbl_tulisan WHERE tulisan_kategori_id ='$hasil->kategori_id'")->row();

                ?>

                        <li class="list-group-item d-flex justify-content-between align-items-center">
                            <a href="<?php echo site_url('blog/kategori/' . str_replace(" ", "-", $hasil->kategori_nama)); ?>" style="text-decoration:none;color:#333;text-transform:uppercase"><i class="fa fa-folder"></i> <?php echo $hasil->kategori_nama ?></a>
                            <span class="badge badge-success badge-pill" style="background-color: <?php echo theme_color_body(); ?>;padding: 6px"><?php echo $query->jumlah ?></span>
                        </li>

                    <?php endforeach; ?>

                <?php elseif (news_ticker() == NULL) : ?>

                    <li><a href="#">Tidak ada data kategori!</a></li>

                <?php endif; ?>
            </ul>


            <!-- end content kanan -->
        </div>
    </div>
</div>