<div class="container">
    <div class="row">
        <div class="col-xs-12 col-md-12">
            <div class="text-center mt-4 mb-5">
                <h2 class="font-weight-bold ">Agenda</h2>
                <p class="text-muted">Berikut Halaman Agenda <?php echo systems('site_title') ?></p>
            </div>
        </div>
        <!-- content kiri -->
        <div class="col-xs-12 col-md-9">
            <div class="row">
                <?php
                if ($data != NULL) :
                    foreach ($data->result() as $hasil) :

                        //limit judul
                        if (strlen($hasil->agenda_nama) < 60) {
                            $judul = $hasil->agenda_nama;
                        } else {
                            $tanggal = $hasil->agenda_mulai;
                            $judul = substr($hasil->agenda_nama, 0, 60) . '...';
                        }

                        //limit tulisan_deskripsi
                        if (strlen($hasil->agenda_deskripsi) < 80) {
                            $agenda_deskripsi = $hasil->agenda_deskripsi;
                        } else {
                            $agenda_deskripsi = substr($hasil->agenda_deskripsi, 0, 80) . '...';
                        }

                ?>
                        <div class="col-md-6">
                            <div class="card border-radius-10 shadow border-0">
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-xl-12">
                                            <div class="widget-49">
                                                <div class="widget-49-title-wrapper">
                                                    <div class="widget-49-date-primary">
                                                        <span class="widget-49-date-day"><?php echo date("d", strtotime($hasil->agenda_mulai)); ?></span>
                                                        <span class="widget-49-date-month"><?php echo date("M", strtotime($hasil->agenda_mulai)); ?></span>
                                                    </div>
                                                    <div class="widget-49-meeting-info">
                                                        <span class="widget-49-pro-title">
                                                            <a href="<?php echo base_url() ?>agenda/<?= $hasil->agenda_id ?>" style="text-decoration:none;color:<?php theme_color_body() ?>">
                                                                <?php echo $judul ?>
                                                            </a>
                                                        </span>
                                                        <span class="widget-49-meeting-time">
                                                            <i class="fa fa-calendar-o"></i>
                                                            <?php echo $this->web->tgl_indo_no_hari($hasil->agenda_tanggal) ?>
                                                        </span>
                                                        <div class="widget-49-meeting-points">
                                                            <?php echo $agenda_deskripsi ?>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    <?php
                    endforeach;
                else :
                    ?>
                    Tidak Agenda Disini
                <?php endif; ?>
                <div class="col-md-12 text-center">
                    <?php echo $page; ?>
                </div>
            </div>
        </div>

        <!-- content kanan -->
        <div class="col-xs-12 col-md-3">
            <div class="col-xs-12 col-md-12 mb-2">
                <div class="row">
                    <h5 class="font-weight-bold mt-1">KATEGORI BERITA</h5>
                </div>
            </div>
            <ul class="list-group">
                <?php
                if (kategori() != NULL) :
                    foreach (kategori() as $hasil) :

                        $query = $this->db->query("SELECT count(tulisan_kategori_id) as jumlah FROM tbl_tulisan WHERE tulisan_kategori_id ='$hasil->kategori_id'")->row();

                ?>

                        <li class="list-group-item d-flex justify-content-between align-items-center">
                            <a href="<?php echo site_url('blog/kategori/' . str_replace(" ", "-", $hasil->kategori_nama)); ?>" style="text-decoration:none;color:#333;text-transform:uppercase"><i class="fa fa-folder"></i> <?php echo $hasil->kategori_nama ?></a>
                            <span class="badge badge-success badge-pill" style="background-color: <?php echo theme_color_body(); ?>;padding: 6px"><?php echo $query->jumlah ?></span>
                        </li>

                    <?php endforeach; ?>

                <?php elseif (news_ticker() == NULL) : ?>

                    <li><a href="#">Tidak ada data kategori!</a></li>

                <?php endif; ?>
            </ul>
        </div>
        <!-- end content kanan -->
    </div>
</div>