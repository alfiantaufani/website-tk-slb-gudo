<div class="container">
    <div class="row">
        <!-- content kiri -->
        <div class="col-xs-12 col-md-12 mt-3">
            <div class="text-center mt-4 mb-5">
                <h2 class="font-weight-bold ">Galeri Video</h2>
                <p class="text-muted">Berikut Halaman Galeri Video <?php echo systems('site_title') ?></p>
            </div>
            <!-- berita baru -->

            <div class="row">
                <?php
                if ($data != NULL) :
                    foreach ($data as $hasil) :
                ?>
                        <div class="col-md-4">
                            <div class="card shadow border-radius-10" style="margin-top:8px;">
                                <iframe style="width: 100%;height: 300px" src="<?php echo $hasil->embed_youtube ?>" frameborder="0" allowfullscreen=""></iframe>
                                <div class="card-body" style="min-height:100px;max-height:100px">
                                    <h4 class="card-title" style="font-size:18px;"><?php echo $hasil->title ?></h4>
                                </div>
                            </div>
                        </div>
                    <?php
                    endforeach;
                else :
                    ?>
                    Tidak Berita Disini
                <?php endif; ?>
            </div>
        </div>
        <!-- end content kiri -->


    </div>
</div>