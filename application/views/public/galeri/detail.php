<div class="container">
    <div class="row">
        <!-- content kiri -->
        <div class="col-xs-12 col-md-12 mt-3">
            <div class="text-center mt-4 mb-5">
                <h2 class="">Galeri Album <b><?= $nama_album->album_nama ?></b></h2>
                <p class="text-muted">Berikut Halaman Galeri Album <b><?= $nama_album->album_nama ?></b> <?php echo systems('site_title') ?></p>
            </div>
            <!-- berita baru -->

            <div class="row">
                <?php
                if ($data != NULL) :
                    foreach ($data as $hasil) :
                ?>
                        <div class="col-md-4">
                            <div class="card border-radius-10" style="margin-top:8px;">
                                <a href="<?php echo base_url() . 'assets/images/' . $hasil->galeri_gambar; ?>" data-lightbox="roadtrip" data-title="<?php echo $hasil->galeri_judul ?>">
                                    <img class="card-img-top" style="object-fit:cover;width:100%;min-height:200px;max-height:200px;border-radius: 10px;" src="<?php echo base_url() . 'assets/images/' . $hasil->galeri_gambar; ?>">
                                </a>
                                <!-- <div class="card-body" style="min-height:100px;max-height:100px">
                                    <h4 class="card-title" style="font-size:18px;"><?php echo $hasil->galeri_judul ?></h4>
                                </div> -->
                            </div>
                        </div>
                        
                    <?php
                    endforeach;
                else :
                    ?>
                    Foto Belum Tersedia
                <?php endif; ?>
            </div>
        </div>
        <!-- end content kiri -->


    </div>
</div>