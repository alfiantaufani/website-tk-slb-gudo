<div class="container">
    <div class="row">
        <!-- content kiri -->
        <div class="col-xs-12 col-md-12 mt-3">
            <div class="text-center mt-4 mb-5">
                <h2 class="font-weight-bold ">Galeri</h2>
                <p class="text-muted">Berikut Halaman Galeri <?php echo systems('site_title') ?></p>
            </div>
            <!-- berita baru -->

            <div class="row">
                <?php
                if ($alb != NULL) :
                    foreach ($alb->result() as $hasil) :
                ?>
                        <!-- <div class="col-md-4">
                            <div class="card" style="margin-top:8px;">
                                <a href="<?php echo base_url() . 'assets/images/' . $hasil->album_cover; ?>" data-lightbox="roadtrip" data-title="<?php echo $hasil->galeri_judul ?>">
                                    <img class="card-img-top" style="object-fit:cover;width:100%;min-height:200px;max-height:200px" src="<?php echo base_url() . 'assets/images/' . $hasil->galeri_gambar; ?>">
                                </a>
                                <div class="card-body" style="min-height:100px;max-height:100px">
                                    <h4 class="card-title" style="font-size:18px;"><?php echo $hasil->album_nama ?></h4>
                                </div>
                            </div>
                        </div> -->
                        <div class="col-md-4">
                            <div class="card shadow border-radius-10" style="margin-top:8px;">
                                <img class="card-img-top" style="object-fit:cover;width:100%;min-height:200px;max-height:200px" src="<?php echo base_url() . 'assets/images/' . $hasil->album_cover; ?>">
                                <div class="card-body" style="min-height:100px;max-height:100px">
                                    <a href="<?php echo base_url('galeri/detail')?>/<?= $hasil->album_id ?>" class="text-decoration-none">
                                        <h4 class="card-title" style="font-size:18px;"><?php echo $hasil->album_nama ?></h4>
                                    </a>
                                </div>
                            </div>
                        </div>
                    <?php
                    endforeach;
                else :
                    ?>
                    Tidak Berita Disini
                <?php endif; ?>
            </div>
        </div>
        <!-- end content kiri -->


    </div>
</div>