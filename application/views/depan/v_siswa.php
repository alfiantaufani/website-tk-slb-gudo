<?php $this->load->view('public/header'); ?>
<!--//END HEADER -->

<section class="our-teachers">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <h2 class="mb-5">Alumni Kami</h2>
      </div>
    </div>
    <div class="row">
      <table id="" class="table table-striped" style="font-size:13px;">
        <thead>
          <tr>
            <th>Photo</th>
            <th>Email</th>
            <th>Nama</th>
            <th>Jenis Kelamin</th>
            <th>Angkatan</th>
          </tr>
        </thead>
        <tbody>
          <?php
          $no = 0;
          foreach ($data->result_array() as $i) :
            $no++;
            $id = $i['siswa_id'];
            $nama = $i['siswa_nama'];
            $jenkel = $i['siswa_jenkel'];
            $photo = $i['siswa_photo'];
            $email = $i['siswa_email'];
            $angakatan = $i['siswa_angkatan'];

          ?>
            <tr>
              <?php if (empty($photo)) : ?>
                <td><img width="40" height="40" class="img-circle" src="<?php echo base_url() . 'assets/images/user_blank.png'; ?>"></td>
              <?php else : ?>
                <td><img width="40" height="40" class="img-circle" src="<?php echo base_url() . 'assets/images/' . $photo; ?>"></td>
              <?php endif; ?>
              <td><?php echo $email; ?></td>
              <td><?php echo $nama; ?></td>
              <?php if ($jenkel == 'L') : ?>
                <td>Laki-Laki</td>
              <?php else : ?>
                <td>Perempuan</td>
              <?php endif; ?>
              <td><?php echo $angakatan; ?></td>

            </tr>
          <?php endforeach; ?>
        </tbody>
      </table>
      <!-- <?php foreach ($data->result() as $row) : ?>
                    <div class="col-xs-12 col-sm-6 col-md-3">
                        <div class="admission_insruction">
                          <?php if (empty($row->siswa_photo)) : ?>
                            <img src="<?php echo base_url() . 'assets/images/blank.png'; ?>" class="img-fluid" alt="#">
                          <?php else : ?>
                            <img src="<?php echo base_url() . 'assets/images/' . $row->siswa_photo; ?>" class="img-fluid" alt="#">
                          <?php endif; ?>
                            <p class="text-center mt-3"><span><?php echo $row->siswa_nama; ?></span>
                                <br>
                                <?php echo $row->siswa_email; ?></p>
                        </div>
                    </div>
                <?php endforeach; ?> -->
    </div>
    <!-- End row -->
    <nav><?php echo $page; ?></nav>
  </div>
</section>

<!--//End Style 2 -->
<!--============================= FOOTER =============================-->
<?php $this->load->view('public/footer'); ?>