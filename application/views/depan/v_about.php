<?php $this->load->view('public/header'); ?>
<!--//END HEADER -->
<!--//END ABOUT IMAGE -->
<!--============================= WELCOME TITLE =============================-->
<!--//END WELCOME TITLE -->
<!--============================= VISI MISI =============================-->
<section class="welcome_about" id="visi">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h2 class="text-center">Visi dan Misi</h2>
            </div>
            <div class="col-md-12">
                <p>
                    <b> Visi : </b> <br>

                    “TERWUJUDNYA GENERASI BERKUALITAS, BERAKHLAKUL KARIMAH, DAN BERWAWASAN LINGKUNGAN ‘’Bedasarkan VISI Pondok Pesantren Darul Ulum yang termaktub dalam al Qur’an SURAH ALI IMRAN AYAT 18 <br> <br>

                    شَهِدَ اللَّهُ أَنَّهُ لاَ إِلـٰهَ إِلاَّ هُوَ وَالْمَلآئِكَةُ وَأُولُوا الْعِلْمِ قَآئِمًا بِالْقِسْطِ لاَ إِلـٰهَ إِلاَّ هُوَ الْعَزِيْزُ الْحَكِيْمُ
                    <br> <br>
                    Artinya : Allah menyatakan bahwasanya tidak ada Tuhan (yang berhak disembah) melainkan Dia, Yang menegakkan keadilan. Para malaikat dan orang-orang yang berilmu (juga menyatakan yang demikian itu). Tak ada Tuhan ( yang berhak disembah) melainkan Dia, Yang Maha Perkasa lagi Maha Bijaksana. QS Ali Imran ayat 18. <br>
                    <br>
                    INDIKATOR-INDIKATOR VISI <br>
                <ul>
                    <li>Prestasi Akademik</li>
                    <li>Prestasi Non Akademik</li>
                    <li> Prestasi dibidang keagamaan</li>
                    <li>Preatasi dibidang Pembiasaan Akhlak</li>
                    <li>Berdaya saing dalam berbagai Kompetisi</li>
                </ul>
                </p>
                <p>
                    <b> Misi :</b> <br>

                    Misi Satuan Pendidikan MTsN 2 Jombang konektifitas dengan misi Pondok Pesantren Darul Ulum Surah Ali Imran Ayat 110 <br> <br>

                    كُنْتُمْ خَيْرَ أُمَّةٍ أُخْرِجَتْ لِلنَّاسِ تَأْمُرُوْنَ بِالْمَعْرُوْفِ وَتَنْهَوْنَ عَنِ الْمُنْكَرِ وَتُؤْمِنُوْنَ بِاللَّهِ وَلَوْ آمَنَ أَهْلُ الْكِتَابِ لَكَانَ خَيْرًا لَهُمْ مِنْهُمُ الْمُؤْمِنُونَ وَأَكْثَرُهُمُ الْفَاسِقُونَ
                    <br><br>
                    Artinya ; Kamu adalah umat yang terbaik yang dilahirkan untuk manusia, menyuruh kepada yang makruf, dan mencegah dari yang mungkar, dan beriman kepada Allah. Sekiranya Ahli Kitab beriman, tentulah itu lebih baik bagi mereka; di antara mereka ada yang beriman, dan kebanyakan mereka adalah orang-orang yang fasik. QS Ali Imran ayat 110. <br>
                </p>
                <p>
                    <b> Penjelasan:</b> <br>

                <ul>
                    <li>Berkualitas artinya bermutu baik/ mempunyai kualitas.Sesuatu itu dikatakan baik/kualitas karena memenuhi kriteria kedalaman dan pengalaman ilmu yang akan menjadikann seseorang itu berkualitas Misalnya: dengan melaksanakan kewajiban yang ada hubungannya dengan Allah dan manusia,serta istiqomah dalam belajar dimadrasah,sholat berjama’ah dan membaca Al-qur’an. (Penafsiran Surah Al- imron ayat 18 dan ayat 110 )</li>
                    <li>
                        Berakhlakul karimah adalah memahami cara berfikir dan berperilaku yang menjadi ciri khas tiap individu untuk hidup dan bekerjasama ,baik dalam lingkungan keluarga ,masyarakat,bangsa dan negara .Individu yang berakhlakul karimah adalah individu yang bisa membuat keputusan dan siap mempertanggungjawabkan tiap akibat dari keputusan yang ia buat.(Ditjen Mandikdasmen-Kementrian pendidikan Nasional).
                    </li>
                    <li>
                        Berwawasan lingkungan adalah :dengan melakukan pengembangan kebijakan sekolah peduli dan berbudaya lingkungan,pengembangan kurikulum berbasis lingkungan,pengembangan kegiatan sekolah berbasis partisipatif dan pengembangan serta pengelolaan pendukung sekolah ramah lingkungan
                    </li>
                </ul>
                </p>
                <p>
                    <b> INDIKATOR-INDIKATOR Misi</b> <br>

                    1. Meningkatkan pembelajaran yang PAIKEM <br>
                    (Pembelajaran, Aktif, Inovatif, Kreatif, Efektif, dan Menyenangkan.) <br> <br>

                    1. Mengembangkan pembelajaran Ekstrakurikuler <br>
                    2. Meningkatkan Program Literasi Madrasah <br>
                    3. Meningkatkan Program Hafid Al-Qur’an (Minimal 2 Juz ) dan baca Kitab <br>
                    4. Membiasakan 5 S ( senyum,salam,sapa,sopan santun) <br>
                    5. Membiasakan hidup bersih,indah dan sehat. <br>
                </p>
                <p>
                    Untuk merealisasikan visi dan misi, MTs Negeri 2 Jombang menerapkan strategi pencapaian sebagai berikut: <br>
                <ul>
                    <li>
                        Mengembangan budaya dan perilaku akademis diantara warga madrasah sehingga tercapai kondisi lingkungan belajar yang dinamis.
                    </li>
                    <li>
                        Mengembangan budaya dan perilaku unggul dalam bingkai profesionalisme dan keteladanan diantara sesama warga madrasah sehingga tercipta kondisi lingkungan belajar yang sejuk, harmonis, dan saling mengedepankan kepentingan lembaga.
                    </li>
                    <li>
                        Menciptakan kondisi lingkungan belajar yang akuntabel, handal, dan secara kontekstual mendukung pembelajaran berbasis kompetensi
                    </li>
                    <li>
                        Mengembangkan layanan pendidikan untuk semua dengan memberi kesempatan seluas-luasnya tenaga pendidik dan tenaga kependidikan serta peserta didik untuk berkembang dan mengembangkan kecakapannya.
                    </li>
                    <li>
                        Mengembangkan potensi peserta didik secara maksimal dalam bentuk pengembangan diri sehingga diperoleh lulusan yang handal dan kompetitif
                    </li>
                    <li>
                        Menyusun tujuan dan melaksanakan tujuan secara konsekuen dengan komitmen maju bersama.
                    </li>
                    <li>
                        Mengembangkan budaya CALISTUNG (baca, tulis, dan berhitung) dan budaya BTHQ ( Baca, tulis ,hifid, Qur’an ) segenap civitas madrasah
                    </li>
                    <li>
                        Meng-upgrade secara berkelanjutan kompetensi pendidik dan tenaga kependidikan yang berwawasan lingkungan
                    </li>
                </ul>
                </p>
                <p>
                    Mengembangkan kemitraan dengan stakeholders dan pihak-pihak luar yang berkepentingan dalam dunia pendidikan <br> <br>
                    <b>Tujuan :</b> <br> <br>
                    Wajib belajar 9 (sembilan) tahun telah dicanangkan Pemerintah sejak hamper 2 dekade silam. Tujuan Wajib Belajar (wajar) 9 tahun adalah untuk meningkatkan mutu dan pendidikan minimal warga Negara Indonesia dari 6 tahun menjadi 9 setahun atau dari tingkat SD/MI menjadi tingkat SMP/MTs. Kemudian program tersebut dikenal dengan Wajar Dikdas 9 tahun. Sehingga tingkat SMP/MTs termasuk kategori pendidikan dasar. Hal tersebut mengacu pada tujuan pendidikan nasional yang berdasarkan Pancasila dan Undang-Undang Dasar Negara Republik Indonesia Tahun 1945 yaitu berfungsi untuk mengembangkan kemampuan dan membentuk watak serta peradaban bangsa yang bermartabat dalam rangka mencerdaskan kehidupan bangsa, bertujuan untuk mengembangkan potensi peserta didik agar menjadi manusia yang beriman dan bertakwa kepada Tuhan Yang Maha Esa, berakhlak mulia, sehat, berilmu, cakap, kreatif, mandiri, dan menjadi warga negara yang demokratis serta bertanggung jawab. Untuk mengemban fungsi tersebut pemerintah menyelenggarakan suatu sistem pendidikan nasional sebagaimana tercantum dalam Undang-Undang Nomor 20 Tahun 2003 tentang Sistem Pendidikan Nasional. <br> <br>

                    Implementasi Undang-Undang Nomor 20 tahun 2003 tentang Sistem Pendidikan Nasional dijabarkan ke dalam sejumlah peraturan antara lain Peraturan Pemerintah Nomor 19 Tahun 2005 tentang Standar Nasional Pendidikan. Peraturan Pemerintah ini memberikan arahan tentang perlunya disusun dan dilaksanakan delapan standar nasional pendidikan, yaitu: standar isi, standar proses, standar kompetensi lulusan, standar pendidik dan tenaga kependidikan, standar sarana dan prasarana, standar pengelolaan, standar pembiayaan, dan standar penilaian pendidikan. <br> <br>

                    Adapun tujuan pendidikan dasar, termasuk MTs Negeri 2 Jombang didalamnya, mengacu pada Peraturan Pemerintah nomor 19 tahun 2005 tentang Standar Nasional Pendidikan adalah untuk meletakkan dasar kecerdasan, pengetahuan, kepribadian, akhlak mulia, serta ketrampilan untuk hidup mandiri dan mengikuti pendidikan lebih lanjut <br> <br>
                </p>

                <p>
                    Selama satu tahun pelajaran Madrasah dapat : <br> <br>
                <ul>
                    <li>
                        Mengembangkan Kurikululm dengan dilengkapi Silabus tiap mata pelajaran, Rencana Pelaksanaan Pembelajaran, Lembar Kegiatan Siswa dan Sistem Penilaian.
                    </li>
                    <li>
                        Mengembangkan program-program pengembangan diri
                    </li>
                    <li>
                        Mengoptimalkan proses pembelajaran dengan pendekatan nonkonvensional di antaranya CTL, Direct Instruction, Cooperative Learning, dan Problem Base Instruction
                    </li>
                    <li>Meningkatkan nilai rata-rata Ujian Nasional minimal sebesar 8,00</li>
                    <li>
                        Mengikutsertakan tenaga pendidik dan tenaga kependidikan dalam pelatihan peningkatan profesionali
                    </li>
                    <li>
                        Memenuhi kebutuhan sarana dan prasarana kegiatan pembelajaran serta sarana penunjang berupa tempat ibadah, kebun madrasah, tempat parkir, kantin sehat madrasah, lapangan olahraga, dan KM sekolah dengan mengedepankan skala prioritas.
                    </li>
                    <li>
                        Melaksanakan Manajemen Berbasis Sekolah dan Manajemen Peningkatan Mutu Berbasis Sekolah secara demokratis, akuntabel, dan terbuka.
                    </li>
                    <li>
                        Menggalang pembiayaan pendididikan secara adil dan demokratis dan memanfaatkan secara terencana serta dipertanggungjawabkan secara jujur, transparan, dan memenuhi akuntabilitas publik.
                    </li>
                    <li>
                        Mengoptimalkan pelaksanaan penilaian autentik secara berkelanjutan
                    </li>
                    <li>
                        Mengoptimalkan pelaksanaan program remedi dan pengayaan
                    </li>
                    <li>
                        Membekali komunitas sekolah agar dapat mengimplementasikan ajaran agama melalui kegiatan shalat berjamaah, baca tulis
                    </li>
                    <li>
                        Alquran, hafalan Surat-surat Pendek / Jus 30.dan jus 1 dalam Al-Qur’an dan pengajian keagamaan.
                    </li>
                    <li>
                        Membentuk kelompok kegiatan bidang Ekstrakurikuler yang bertaraf lokal, regional maupun nasional.
                    </li>
                    <li>
                        Mengikutsertakan siswa dalam kegiatan Porseni tingkat Kabupaten atau jenjang berikutnya.
                    </li>
                    <li>
                        Memiliki tim olah raga yang dapat bersaing pada tingkat kabupaten atau jenjang berikutnya.
                    </li>
                    <li>
                        Memiliki Gudep Pramuka yang dapat berperan serta secara aktif dalam Jambore Daerah, serta even kepramukaan lainnya.
                    </li>
                    <li>
                        Menanamkan sikap santun, berbudi pekerti luhur dan berbudaya, budaya hidup sehat, cinta kebersihan, cinta kelestarian lingkungan dengan dilandasi keimanan dan ketakwaan terhadap Tuhan yang Maha Esa.
                    </li>
                    <li>
                        Memanfaatkan lingkungan sekitar antara lain pengelolaan kantin sehat
                    </li>
                </ul>
                </p>
            </div>
        </div>
    </div>
</section>
<!--//END VISI MISI -->

<!-- SEJARAH SINGKAT -->
<section class="testimonial" id="sejarah">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h2 class="text-center">Sejarah Singkat</h2>
            </div>
            <div class="col-md-12">
                <p>
                    Pondok Pesantren Darul 'Ulum Rejoso Peterongan Jombang adalah suatu lembaga keagamaan yang didirikan oleh KH. Tamim Irsyad dari Madura pada tahun 1885 M. Beliau merupakan pembabat tanah pertama di desa Rejoso, kemudian diteruskan oleh putra dan cucu beliau sampai sekarang.
                </p>
                <p>
                    Pada saat itu Pondok Pesantren Darul 'Ulum Rejoso Peterongan Jombang memiliki lembaga pendidikan dari tingkat MI sampai dengan tingkat lanjutan. Saat itu nama pendidikan tingkat lanjutan adalah Madrasah Muallimin (putra) dan Madrasah Muallimat (putri).
                </p>
                <p>
                    Dalam perkembangan zaman tahun 1963 Madrasah Muallimin tersebut diubah menjadi Pendidikan Muallimin Pertama (PMP) untuk tingkat SMP dan Pendidikan Muallimin Atas (PMA), yang akhirnya baik PMP maupun PMA keduanya untuk murid putra dan putri.
                </p>
                <p>
                    Pada tahun 1968, KH. Moh. Dahlan sebagai Menteri Agama Republik Indonesia mengatakan bahwa Pendidikan Agama Swasta, memberikan peluang apabila menginginkan merubah status dari swasta ke negeri. Maka Pondok Pesantren Darul 'Ulum mengambil kebijaksanaan menegerikan sekolah-sekolah yang ada di lingkungan Departemen Agama. Dari MI sampai MA dan dari Pendidikan Muallimin Pertama (PMP) ke MTsAIN dan dari Pendidikan Muallimin Atas (PMA) ke MAAIN dan PGAN 4th. Penegerian MTsAIN dan MAAIN untuk murid tahun pertama putri saja, sedangkan murid putra tingkat pertama menjadi SMP dan tingkat atas menjadi SMA.
                </p>
                <p>
                    Dua tahun berikutnya dibuka MTsAIN dan MAAIN untuk putra dan putri. Pada tahun 1978 sesuai dengan keputusan Menteri Agama Republik Indonesia, maka MTsAIN diganti namanya menjadi MTsN Rejoso Peterongan 1 dan penghapusan PGAN 4 tahun menjadi MTsN Rejoso II.
                </p>
                <p>
                    Pada saat MTsN Rejoso II dialokasikan ke Karang Asem Bali, maka murid dan gurunya dimutasikan ke MTsN Rejoso Peterongan I Jombang.
                </p>
                <p>
                    Dalam perkembangannya, Madrasah Tsanawiyah yang pertama didirikan ini, banyak tantangan dan hambatan, seperti belum memiliki gedung dan guru sendiri. Namun pada akhirnya Madrasah Tsanawiyah ini berkembang pesat.
                </p>
                <p>
                    Tahun 1983 MTsN Rejoso mendapatkan proyek pembangunan gedung sebelah utara dari kantor ( lihat deanah sekolah ). Tahun 1992 mendapat proyek pembangunan gedung 4 lokal sebelah selatan kantor, dan pada tahun 2003 mendapat proyek bangunan gedung 3 lokal yang di bangun di depan laboratorium computer.
                </p>
                <p>
                    Kepala Madrasah yang pernah menduduki jabatan sebagai Kepala MTsN 2 Jombang, diantaranya : <br> <br>
                <ul>
                    <li>Bapak Baiduri Luqman (1968 s.d 1977 )</li>
                    <li>Bapak Kasijan ( 1978 s.d 1995)</li>
                    <li>
                        Bapak Abu Mansyur ( 1995 s.d 1997 )
                    </li>
                    <li>
                        Bapak A. Rifa’i Dimyathi, SH. ( 1997 s.d 1999 )
                    </li>
                    <li>
                        Ibu Hj. Umi Sa’adah ( 1999 s.d 2008 )
                    </li>
                    <li>
                        Ibu Mulyaningsih Sri Andayani, S.Pd ( 2008 s.d sekarang)
                    </li>
                </ul>
                </p>
            </div>
        </div>
        <div>
</section>
<!-- END SEJARAH SINGKAT -->

<!-- SARPRAS -->
<section class="welcome_about" id="sarpras">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h2 class="text-center">Sarana & Prasarana</h2>
            </div>
            <div class="col-md-12">
                <p>
                    Berikut ini adalah sarana dan prasarana yang ada di sekolah : <br> <br>
                    <img src="<?php echo base_url() . 'theme/images/sarpras.png' ?>" alt="" width="10px">
                </p>
            </div>
        </div>
    </div>
</section>
<!-- END SARPRAS -->

<!-- Struktur Organisasi -->
<section class="testimonial" id="struktur-organisasi">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h2 class="text-center">Struktur Organisasi</h2>
            </div>
            <div class="col-md-12">
                <p>
                    Struktur organisasi sekolah akan kami update disini
                </p>
            </div>
        </div>
    </div>
</section>
<!-- Struktur Organisasi -->

<!-- Kepala Sekolah -->
<section class="welcome_about" id="kepsek">
    <div class="container">
        <div class="row">
            <div class="col-md-3">
                <img src="<?php echo base_url() . 'theme/images/kepsek/kepsek2.jpg' ?>" class="img-fluid" alt="#">
            </div>
            <div class="col-md-9">
                <h2>Kepala Madrasah</h2>
                <p>
                    <b>Mulyaningsih Sri Handayani, S.Pd., M.Pd.. </b> adalah kepala Mts Negeri 2 Jombang yang ke 6 sejak berdirinya sekolah ini pada tahun 1978. Lahir di Jombnag pada tahun 1964. Menghabiskan masa kecil sampai selesai tingkat SLTA di Jombang. Ada beberapa perguruan tinggi yang ditempuh oleh beliau yaitu perguruan tinggi D3 IAIN Sunan Ampel pada tahun 1986 , S1 BK di undar membawanya menjadi seorang sarjana Bimbingan Konseling pada tahun 2002 dan Jurusan Bahasa Indonesia di Universitas Kanjuruan Malang membawanya menjadi seorang sarjana Bahasa Indonesia pada tahun 2010. Magister Manajemen Pendidikan diselesaikannya pada tahun 2012 di Universitas pondok pesantren darul ulum Jombang.
                </p>
            </div>

        </div>
    </div>
</section>
<!-- End Kepala Sekolah -->

<!-- Program Kerja -->
<section class="testimonial" id="program-kerja">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h2 class="text-center">Program Kerja</h2>
            </div>
            <div class="col-md-12">
                <p>
                    <b>Program Unggulan</b> <br> <br>
                <ul>
                    <li>Menjadi Sekolah Standar Nasional (SSN)</li>
                    <li>Mengembangkan Sikap dan Kompetensi Keagamaan</li>
                    <li>Mengembangkan Potensi Siswa Berbasis Multiple Intelligance</li>
                    <li>Mengembangkan Budaya daerah</li>
                    <li>Mengembangkan Kemampuan bahasa dan Teknologi Informasi</li>
                    <li>Meningkatkan Daya serap Ke Dunia Kerja</li>
                </ul>
                </p>
                <p>
                    <b>Program Pengembangan Sarana Prioritas</b>
                <ul>
                    <li>Membangun 5 Ruang kelas Belajar dengan konstruksi bangunan 3 tingkat</li>
                    <li>Membangun 1 ruang Belajar di lantai 2 gedung lama</li>
                    <li>Membangun Ruang Lab Praktek 3 buah</li>
                    <li>Pembangunan Kantin Siswa</li>
                    <li>Perbaikan dan Pengecetan Lapangan Olah Raga</li>
                    <li>Pengembangan Jaringan Infrastruktur LAN (Intranet dan Internet)</li>
                    <li>Pengembangan Sistem Informasi Sekolah (SIS)</li>
                    <li>Melengkapi Sarana dan Prasarana Perpustakaan dan Lab Komputer</li>
                    <li>Renovasi Aula</li>
                    <li>Renovasi Tampilan Depan Skolah/Gerbang Sekolah</li>
                    <li>Melengkapi alat praktek</li>
                </ul>
                </p>
            </div>
        </div>
    </div>
</section>
<!-- Program Kerja -->

<!-- Komite Sekolah -->
<section class="welcome_about" id="komite-sekolah">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h2>Komite Sekolah</h2>
                <p>
                    Semenjak diluncurkannya konsep Manajemen Peningkatan Mutu Berbasis Sekolah dalam sistem manajemen sekolah, Komite Sekolah sebagai organisasi mitra sekolah memiliki peran yang sangat strategis dalam upaya turut serta mengembangkan pendidikan di sekolah. Kehadirannya tidak hanya sekedar sebagai stempel sekolah semata, khususnya dalam upaya memungut biaya dari orang tua siswa, namun lebih jauh Komite Sekolah harus dapat menjadi sebuah organisasi yang benar-benar dapat mewadahi dan menyalurkan aspirasi serta prakarsa dari masyarakat dalam melahirkan kebijakan operasional dan program pendidikan di sekolah serta dapat menciptakan suasana dan kondisi transparan, akuntabel, dan demokratis dalam penyelenggaraan dan pelayanan pendidikan yang bermutu di sekolah.
                </p>
                <p>
                    Agar Komite Sekolah dapat berdaya, maka dalam pembentukan pengurus pun harus dapat memenuhi beberapa prinsip/kaidah dan mekanisme yang benar, serta dapat dikelola secara benar pula. Susunan pengurus Komite Sekolah adalah sebagai berikut:
                </p>
                <p>
                    Ketua : Drs. KH. Chozin Dahlan, MSi <br>
                    Sekretaris : M. Nurhadi <br>
                    Bendahara : Uswatun Khasanah <br>
                    <br>
                    Anggota 1 : Drs. Hadi Saifuddin, M.MPd <br>
                    Anggota 2 : Lailatus Sholichah <br>
                    Anggota 3 : Kasban <br>
                    Anggota 4 : Ainun Jariyah <br>
                </p>
            </div>
        </div>
    </div>
</section>
<!-- End Komite Sekolah -->

<!-- Prestasi siswa -->
<section class="testimonial" id="prestasi-siswa">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h2 class="text-center">Prestasi Siswa</h2>
            </div>
            <div class="col-md-12">
                <img src="<?php echo base_url() . 'theme/images/prestasi1.png' ?>" alt="" width="100%">
            </div>
        </div>
    </div>
</section>
<!-- Prestasi siswa -->

<!--============================= DETAILED CHART =============================-->
<div class="detailed_chart">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-6 col-md-3 chart_bottom">
                <div class="chart-img">
                    <img src="<?php echo base_url() . 'theme/images/chart-icon_1.png' ?>" class="img-fluid" alt="chart_icon">
                </div>
                <div class="chart-text">
                    <p><span class="counter"><?php echo $tot_guru; ?></span> Guru
                    </p>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-3 chart_bottom chart_top">
                <div class="chart-img">
                    <img src="<?php echo base_url() . 'theme/images/chart-icon_2.png' ?>" class="img-fluid" alt="chart_icon">
                </div>
                <div class="chart-text">
                    <p><span class="counter"><?php echo $tot_siswa; ?></span> Siswa
                    </p>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-3 chart_top">
                <div class="chart-img">
                    <img src="<?php echo base_url() . 'theme/images/chart-icon_3.png' ?>" class="img-fluid" alt="chart_icon">
                </div>
                <div class="chart-text">
                    <p><span class="counter"><?php echo $tot_files; ?></span> Download
                    </p>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-3">
                <div class="chart-img">
                    <img src="<?php echo base_url() . 'theme/images/chart-icon_4.png' ?>" class="img-fluid" alt="chart_icon">
                </div>
                <div class="chart-text">
                    <p><span class="counter"><?php echo $tot_agenda; ?></span> Agenda</p>
                </div>
            </div>
        </div>
    </div>
</div>
<!--//END DETAILED CHART -->

<?php $this->load->view('public/footer'); ?>