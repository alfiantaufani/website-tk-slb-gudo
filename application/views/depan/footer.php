<?php $query = $this->db->query("SELECT * FROM tbl_setting WHERE id=1")->result_array(); foreach ($query as $footer) :?>
<footer>
    <div class="container">
        <div class="row">
            <div class="col-md-3">
                <div class="foot-logo text-center">
                    <a href="<?php echo site_url();?>" class="">
                        <img src="<?php echo base_url().'theme/images/logo/logo.png'?>" class="img-fluid" alt="footer_logo" width="100px">
                    </a>
                    <p><?= $footer['description'] ?></p>
                </div>
            </div>
            <div class="col-md-3">
                <div class="sitemap">
                    <h3>Menu Utama</h3>
                    <ul>
                        <li><a href="<?php echo site_url();?>">Home</a></li>
                        <li><a href="<?php echo site_url('p/sejarah-singkat');?>">Profil & Sejarah Singkat</a></li>
                        <li><a href="<?php echo site_url('artikel');?>">Artikel </a></li>
                        <li><a href="<?php echo site_url('galeri');?>">Galeri</a></li>
                        <li><a href="<?php echo site_url('contact');?>">Kontak Kami</a></li>
                    </ul>
                </div>
            </div>
            <div class="col-md-3">
                <div class="sitemap">
                    <h3>Akademik</h3>
                    <ul>
                        <li><a href="<?php echo site_url('guru');?>">Guru</a></li>
                        <li><a href="<?php echo site_url('siswa');?>">Alumni </a></li>
                        <li><a href="<?php echo site_url('pengumuman');?>">Pengumuman</a></li>
                        <li><a href="<?php echo site_url('agenda');?>">Agenda</a></li>
                        <li><a href="<?php echo site_url('download');?>">Download</a></li>
                        <li><a href="https://mtsn2jombang.com/rdm">Rapor Digital Madrasah (RDM)</a></li>
                    </ul>
                </div>
            </div>
            <div class="col-md-3">
                <div class="address">
                    <h3>Hubungi Kami</h3>
                    <p><span>Alamat: </span> <?= $footer['address'] ?></p>
                    <p>Email : <?= $footer['email'] ?>
                        <br> Phone : <?= $footer['no_telephone'] ?>
                    </p>
                        <ul class="footer-social-icons">
                            <li><a href="<?= $footer['facebook'] ?>"><i class="fa fa-facebook fa-fb" aria-hidden="true"></i></a></li>
                            <li><a href="<?= $footer['instagram'] ?>"><i class="fa fa-instagram fa-in" aria-hidden="true"></i></a></li>
                            <li><a href="<?= $footer['twitter'] ?>"><i class="fa fa-twitter fa-tw" aria-hidden="true"></i></a></li>
                        </ul>
                </div>
            </div>
            <div class="col-md-12 mt-4">
                <div class="address text-center">
                    <p><?= $footer['site_footer'] ?></p>
                </div>
            </div>
        </div>
    </div>
</footer>
<?php endforeach ?>
<!--//END FOOTER -->
<!-- jQuery, Bootstrap JS. -->
<script src="<?php echo base_url().'theme/js/jquery.min.js'?>"></script>
<script src="<?php echo base_url().'theme/js/tether.min.js'?>"></script>
<script src="<?php echo base_url().'theme/js/bootstrap.min.js'?>"></script>
<!-- Plugins -->
<script src="<?php echo base_url().'theme/js/slick.min.js'?>"></script>
<script src="<?php echo base_url().'theme/js/waypoints.min.js'?>"></script>
<script src="<?php echo base_url().'theme/js/counterup.min.js'?>"></script>
<script src="<?php echo base_url().'theme/js/owl.carousel.min.js'?>"></script>
<script src="<?php echo base_url().'theme/js/validate.js'?>"></script>
<script src="<?php echo base_url().'theme/js/tweetie.min.js'?>"></script>
<!-- Subscribe -->
<script src="<?php echo base_url().'theme/js/subscribe.js'?>"></script>

<script src="<?php echo base_url().'theme/js/jquery-ui-1.10.4.min.js'?>"></script>
<script src="<?php echo base_url().'theme/js/jquery.isotope.min.js'?>"></script>
<script src="<?php echo base_url().'theme/js/animated-masonry-gallery.js'?>"></script>
<!-- Magnific popup JS -->
<script src="<?php echo base_url().'theme/js/jquery.magnific-popup.js'?>"></script>

<!-- Script JS -->
<script src="<?php echo base_url().'theme/js/script.js'?>"></script>
<script src="<?php echo base_url().'theme/js/jssocials.js'?>"></script>
        <script>
          $(document).ready(function(){
            $(".sharePopup").jsSocials({
                  showCount: true,
            			showLabel: true,
            			shareIn: "popup",
            			shares: [
            			{ share: "twitter", label: "Twitter" },
            			{ share: "facebook", label: "Facebook" },
            			{ share: "googleplus", label: "Google+" },
            			{ share: "whatsapp", label: "Whatsapp" }
            			]
                });
          });
        </script>

</body>

</html>
