<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * @package	:	Website Sekolah
 * @author	:	Fika Ridaul Maulayya (https://www.rubypedia.com)
 * @version	:	Beta Version V.0.1
 * @license	:	Protection
 */
if(!function_exists('kategori'))
{
    function kategori()
    {
        $CI =& get_instance();

        $query = $CI->db->select('*')->from('tbl_kategori')->order_by('kategori_nama','ASC')->get();

        if($query->num_rows() < 0){

            return NULL;
        }else{
            return $query->result();
        }
    }
}