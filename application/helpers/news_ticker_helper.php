<?php
defined('BASEPATH') or exit('No direct script access allowed');
/**
 * @package	:	Website Sekolah
 * @author	:	Fika Ridaul Maulayya (https://www.rubypedia.com)
 * @version	:	Beta Version V.0.1
 * @license	:	Protection
 */
if (!function_exists('news_ticker')) {
    function news_ticker()
    {
        $CI = &get_instance();

        $query = $CI->db->select('*')->from('tbl_tulisan')->order_by('tulisan_id', 'DESC')->limit(5, 0)->get();

        if ($query->num_rows() < 0) {

            return NULL;
        } else {
            return $query->result();
        }
    }
}
