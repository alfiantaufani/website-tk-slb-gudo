<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * @package	:	Website Sekolah
 * @author	:	Fika Ridaul Maulayya (https://www.rubypedia.com)
 * @version	:	Beta Version V.0.1
 * @license	:	Protection
 */
if ( ! function_exists('theme_color_navbar'))
{
    function theme_color_navbar()
    {
        echo '#4496D2';
    }
}

if ( ! function_exists('theme_color_border_navbar'))
{
    function theme_color_border_navbar()
    {
        echo '5px solid #4496D2';
    }
}

if ( ! function_exists('theme_color_header'))
{
    function theme_color_header()
    {
        echo '#4496D2';
    }
}

if ( ! function_exists('theme_color_body'))
{
    function theme_color_body()
    {
        echo '#4496D2';
    }
}

if ( ! function_exists('theme_color_footer'))
{
    function theme_color_footer()
    {
        echo '#43494c';
    }
}

if ( ! function_exists('theme_color_copyright'))
{
    function theme_color_copyright()
    {
        echo '#262a2d';
    }
}