<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * @package	:	Website Sekolah
 * @author	:	Fika Ridaul Maulayya (https://www.rubypedia.com)
 * @version	:	Beta Version V.0.1
 * @license	:	Protection
 */
if(!function_exists('systems'))
{
    function systems($key)
    {
        $CI =& get_instance();

        $query = $CI->db->select($key)->where('id',1)->get('tbl_setting');

        if($query->num_rows() != 1){

            return NULL;
        }else{
            $result = $query->row();

            return $result->$key;
        }
    }
}