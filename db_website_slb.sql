-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 12 Des 2022 pada 16.13
-- Versi server: 10.4.19-MariaDB
-- Versi PHP: 7.4.20

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_website_slb`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_agenda`
--

CREATE TABLE `tbl_agenda` (
  `agenda_id` int(11) NOT NULL,
  `agenda_nama` varchar(200) DEFAULT NULL,
  `image` varchar(225) DEFAULT NULL,
  `agenda_tanggal` timestamp NULL DEFAULT current_timestamp(),
  `agenda_deskripsi` text DEFAULT NULL,
  `agenda_mulai` date DEFAULT NULL,
  `agenda_selesai` date DEFAULT NULL,
  `agenda_tempat` varchar(90) DEFAULT NULL,
  `agenda_waktu` varchar(30) DEFAULT NULL,
  `agenda_keterangan` varchar(200) DEFAULT NULL,
  `agenda_author` varchar(60) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_album`
--

CREATE TABLE `tbl_album` (
  `album_id` int(11) NOT NULL,
  `album_nama` varchar(50) DEFAULT NULL,
  `album_tanggal` timestamp NULL DEFAULT current_timestamp(),
  `album_pengguna_id` int(11) DEFAULT NULL,
  `album_author` varchar(60) DEFAULT NULL,
  `album_count` int(11) DEFAULT 0,
  `album_cover` varchar(40) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tbl_album`
--

INSERT INTO `tbl_album` (`album_id`, `album_nama`, `album_tanggal`, `album_pengguna_id`, `album_author`, `album_count`, `album_cover`) VALUES
(5, 'Siswa', '2022-12-12 11:52:25', 8, 'Administrator', 1, '7d910cbe30a8bed2bf97ff28ee70b61c.png');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_files`
--

CREATE TABLE `tbl_files` (
  `file_id` int(11) NOT NULL,
  `file_judul` varchar(120) DEFAULT NULL,
  `file_deskripsi` text DEFAULT NULL,
  `file_tanggal` timestamp NULL DEFAULT current_timestamp(),
  `file_oleh` varchar(60) DEFAULT NULL,
  `file_download` int(11) DEFAULT 0,
  `file_data` varchar(120) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_galeri`
--

CREATE TABLE `tbl_galeri` (
  `galeri_id` int(11) NOT NULL,
  `galeri_judul` varchar(60) DEFAULT NULL,
  `galeri_tanggal` timestamp NULL DEFAULT current_timestamp(),
  `galeri_gambar` varchar(40) DEFAULT NULL,
  `galeri_album_id` int(11) DEFAULT NULL,
  `galeri_pengguna_id` int(11) DEFAULT NULL,
  `galeri_author` varchar(60) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tbl_galeri`
--

INSERT INTO `tbl_galeri` (`galeri_id`, `galeri_judul`, `galeri_tanggal`, `galeri_gambar`, `galeri_album_id`, `galeri_pengguna_id`, `galeri_author`) VALUES
(20, 'Siswa Siswi', '2022-12-12 13:27:18', 'fc52659c9c0f6d290b17cef417186224.png', 5, 8, 'Administrator');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_guru`
--

CREATE TABLE `tbl_guru` (
  `guru_id` int(11) NOT NULL,
  `guru_nip` varchar(30) DEFAULT NULL,
  `guru_nama` varchar(70) DEFAULT NULL,
  `guru_jenkel` varchar(2) DEFAULT NULL,
  `guru_tmp_lahir` varchar(80) DEFAULT NULL,
  `guru_tgl_lahir` varchar(80) DEFAULT NULL,
  `guru_mapel` varchar(120) DEFAULT NULL,
  `guru_photo` varchar(40) DEFAULT NULL,
  `guru_tgl_input` timestamp NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_inbox`
--

CREATE TABLE `tbl_inbox` (
  `inbox_id` int(11) NOT NULL,
  `inbox_nama` varchar(40) DEFAULT NULL,
  `inbox_email` varchar(60) DEFAULT NULL,
  `inbox_kontak` varchar(20) DEFAULT NULL,
  `inbox_pesan` text DEFAULT NULL,
  `inbox_tanggal` timestamp NULL DEFAULT current_timestamp(),
  `inbox_status` int(11) DEFAULT 1 COMMENT '1=Belum dilihat, 0=Telah dilihat'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_info_ppdb`
--

CREATE TABLE `tbl_info_ppdb` (
  `id` int(11) NOT NULL,
  `description` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `tbl_info_ppdb`
--

INSERT INTO `tbl_info_ppdb` (`id`, `description`) VALUES
(1, '<p>Alamat Sekolah :&nbsp;<br />Jl. Raya Sukopinggir - Bugasur Kedaleman No.09, Bugasur, Bugasurkedaleman, Kec. Gudo, Kabupaten Jombang, Jawa Timur 61463</p>\r\n<p>Telephone : 081334550766<br /><br />Alamat Email:<br /><a href=\"mailto:slbsunaraji8278@gmail.com\">slbsunaraji8278@gmail.com</a></p>\r\n<p>Info Lanjut:</p>');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_kategori`
--

CREATE TABLE `tbl_kategori` (
  `kategori_id` int(11) NOT NULL,
  `kategori_nama` varchar(30) DEFAULT NULL,
  `kategori_tanggal` timestamp NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tbl_kategori`
--

INSERT INTO `tbl_kategori` (`kategori_id`, `kategori_nama`, `kategori_tanggal`) VALUES
(1, 'Pendidikan', '2016-09-06 05:49:04'),
(2, 'Politik', '2016-09-06 05:50:01'),
(3, 'Sains', '2016-09-06 05:59:39'),
(5, 'Penelitian', '2016-09-06 06:19:26'),
(6, 'Prestasi', '2016-09-07 02:51:09'),
(13, 'Olah Raga', '2017-01-13 13:20:31'),
(14, 'Kepondokan', '2021-08-13 15:24:08'),
(15, 'Berita', '2022-12-07 13:36:54');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_kelas`
--

CREATE TABLE `tbl_kelas` (
  `kelas_id` int(11) NOT NULL,
  `kelas_nama` varchar(40) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_komentar`
--

CREATE TABLE `tbl_komentar` (
  `komentar_id` int(11) NOT NULL,
  `komentar_nama` varchar(30) DEFAULT NULL,
  `komentar_email` varchar(50) DEFAULT NULL,
  `komentar_isi` varchar(120) DEFAULT NULL,
  `komentar_tanggal` timestamp NULL DEFAULT current_timestamp(),
  `komentar_status` varchar(2) DEFAULT NULL,
  `komentar_tulisan_id` int(11) DEFAULT NULL,
  `komentar_parent` int(11) DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_log_aktivitas`
--

CREATE TABLE `tbl_log_aktivitas` (
  `log_id` int(11) NOT NULL,
  `log_nama` text DEFAULT NULL,
  `log_tanggal` timestamp NULL DEFAULT current_timestamp(),
  `log_ip` varchar(20) DEFAULT NULL,
  `log_pengguna_id` int(11) DEFAULT NULL,
  `log_icon` blob DEFAULT NULL,
  `log_jenis_icon` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_menu`
--

CREATE TABLE `tbl_menu` (
  `id` int(11) NOT NULL,
  `menu_name` varchar(225) NOT NULL,
  `menu_link` varchar(225) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `tbl_menu`
--

INSERT INTO `tbl_menu` (`id`, `menu_name`, `menu_link`) VALUES
(1, 'HOME', 'home'),
(12, 'TENTANG', '#'),
(13, 'INFO SEKOLAH', '#'),
(16, 'BERITA', 'blog/kategori/Berita'),
(17, 'GURU', 'guru/'),
(18, 'GALERI', '#'),
(19, 'PPDB', 'ppdb/'),
(20, 'KONTAK', 'contact/');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_menu_ppdb`
--

CREATE TABLE `tbl_menu_ppdb` (
  `id` int(11) NOT NULL,
  `menu_name` varchar(225) NOT NULL,
  `menu_link` varchar(225) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `tbl_menu_ppdb`
--

INSERT INTO `tbl_menu_ppdb` (`id`, `menu_name`, `menu_link`) VALUES
(1, 'ALUR PENDAFTARAN', 'p/alur-pendaftaran'),
(3, 'SYARAT PENDAFTARAN', 'p/syarat-pendaftaran'),
(4, 'PENGUMUMAN PPDB', 'p/pengumuman-ppdb'),
(5, 'FORMULIR PENDAFTARAN', 'p/formulir-pendaftaran');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_page`
--

CREATE TABLE `tbl_page` (
  `id` int(11) NOT NULL,
  `title` varchar(225) NOT NULL,
  `slug` varchar(225) NOT NULL,
  `content` text NOT NULL,
  `description` text NOT NULL,
  `user_id` varchar(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `tbl_page`
--

INSERT INTO `tbl_page` (`id`, `title`, `slug`, `content`, `description`, `user_id`) VALUES
(29, 'Visi Dan Misi', 'visi-dan-misi', '<p><sup>Halaman Visi Misi</sup></p>\r\n<h2><strong>VISI</strong></h2>\r\n<p>...</p>\r\n<h2><strong>MISI</strong></h2>\r\n<ul>\r\n<li>\r\n<p>...</p>\r\n</li>\r\n<li>\r\n<p>...</p>\r\n</li>\r\n</ul>', 'Visi Dan Misi', '8'),
(30, 'Sejarah', 'sejarah', '<p>Halaman Sejarah</p>', 'Halaman Sejarah', '8'),
(32, 'Sambutan Kepala Sekolah', 'sambutan-kepala-sekolah', '<p>Halaman Sambutan Kepala Sekolah</p>', 'Halaman Sambutan Kepala Sekolah', '8'),
(33, 'Pimpinan & Struktur', 'pimpinan-struktur', '<p>Halaman Pimpinan &amp; Struktur</p>', 'Halaman Pimpinan & Struktur', '8'),
(34, 'Tenaga Pendidik Kependidikan', 'tenaga-pendidik-kependidikan', '<p>Halaman Tenaga Pendidik Kependidikan</p>', 'Tenaga Pendidik Kependidikan', '8'),
(35, 'Vokasi', 'vokasi', '<p>Halaman Vokasi</p>', 'Vokasi', '8'),
(36, 'Program Unggulan', 'program-unggulan', '<p>Halaman Program Unggulan</p>', 'Program Unggulan', '8'),
(37, 'P5', 'p5', '<p>Halaman P5</p>', 'Halaman P5', '8'),
(38, 'Bina Diri', 'bina-diri', '<p>Halaman Bina Diri</p>', 'Bina Diri', '8'),
(39, 'Fasilitas', 'fasilitas', '<p>Halaman Fasilitas</p>', 'Fasilitas', '8'),
(40, 'Video Profile', 'video-profile', '<p>Halaman Video Profile</p>', 'Video Profile', '8'),
(41, 'Alur Pendaftaran', 'alur-pendaftaran', '<p>Halaman Alur Pendaftaran</p>', 'Halaman Alur Pendaftaran', '8'),
(42, 'SYARAT PENDAFTARAN', 'syarat-pendaftaran', '<p>Halaman SYARAT PENDAFTARAN</p>', 'SYARAT PENDAFTARAN', '8'),
(43, 'PENGUMUMAN PPDB', 'pengumuman-ppdb', '<p>Halaman PENGUMUMAN PPDB</p>', 'PENGUMUMAN PPDB', '8'),
(44, 'FORMULIR PENDAFTARAN', 'formulir-pendaftaran', '<p>Silahkan klik tombol di bawah ini</p>\r\n<p><a href=\"https://docs.google.com/forms/d/e/1FAIpQLSdJYcJZ_bDmpnTl6WVemuL1wazehtliqj4TINtn8TGoCwn9bA/viewform\">Daftar Disini</a></p>', 'FORMULIR PENDAFTARAN', '8');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_pengguna`
--

CREATE TABLE `tbl_pengguna` (
  `pengguna_id` int(11) NOT NULL,
  `pengguna_nama` varchar(50) DEFAULT NULL,
  `pengguna_moto` varchar(100) DEFAULT NULL,
  `pengguna_jenkel` varchar(2) DEFAULT NULL,
  `pengguna_username` varchar(30) DEFAULT NULL,
  `pengguna_password` varchar(35) DEFAULT NULL,
  `pengguna_tentang` text DEFAULT NULL,
  `pengguna_email` varchar(50) DEFAULT NULL,
  `pengguna_nohp` varchar(20) DEFAULT NULL,
  `pengguna_facebook` varchar(35) DEFAULT NULL,
  `pengguna_twitter` varchar(35) DEFAULT NULL,
  `pengguna_linkdin` varchar(35) DEFAULT NULL,
  `pengguna_google_plus` varchar(35) DEFAULT NULL,
  `pengguna_status` int(2) DEFAULT 1,
  `pengguna_level` varchar(3) DEFAULT NULL,
  `pengguna_register` timestamp NULL DEFAULT current_timestamp(),
  `pengguna_photo` varchar(40) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tbl_pengguna`
--

INSERT INTO `tbl_pengguna` (`pengguna_id`, `pengguna_nama`, `pengguna_moto`, `pengguna_jenkel`, `pengguna_username`, `pengguna_password`, `pengguna_tentang`, `pengguna_email`, `pengguna_nohp`, `pengguna_facebook`, `pengguna_twitter`, `pengguna_linkdin`, `pengguna_google_plus`, `pengguna_status`, `pengguna_level`, `pengguna_register`, `pengguna_photo`) VALUES
(7, 'Bismalabs', NULL, 'L', 'bismalabs', '6e47784c58e5543ec71ba5a16a43e66f', NULL, 'bismalabs@gmail.com', '081383167850', NULL, NULL, NULL, NULL, 1, '1', '2022-12-06 12:42:27', '0fc312e933f77f1ea911259a3b18e021.png'),
(8, 'Administrator', NULL, 'L', 'admin', '21232f297a57a5a743894a0e4a801fc3', NULL, 'email@gmai.com', '081234567890', NULL, NULL, NULL, NULL, 1, '1', '2022-12-06 12:47:27', 'a3b2f5ea7d5d9eb037eb6611ed60244f.png');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_pengumuman`
--

CREATE TABLE `tbl_pengumuman` (
  `pengumuman_id` int(11) NOT NULL,
  `pengumuman_judul` varchar(150) DEFAULT NULL,
  `pengumuman_deskripsi` text DEFAULT NULL,
  `pengumuman_tanggal` timestamp NULL DEFAULT current_timestamp(),
  `pengumuman_author` varchar(60) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_pengunjung`
--

CREATE TABLE `tbl_pengunjung` (
  `pengunjung_id` int(11) NOT NULL,
  `pengunjung_tanggal` timestamp NULL DEFAULT current_timestamp(),
  `pengunjung_ip` varchar(40) DEFAULT NULL,
  `pengunjung_perangkat` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_setting`
--

CREATE TABLE `tbl_setting` (
  `id` int(11) NOT NULL,
  `admin_title` varchar(225) NOT NULL,
  `admin_footer` varchar(225) NOT NULL,
  `site_title` varchar(225) NOT NULL,
  `site_footer` varchar(225) NOT NULL,
  `logo` varchar(225) NOT NULL,
  `foto_kepsek` varchar(225) NOT NULL,
  `nama_kepsek` varchar(225) NOT NULL,
  `text_home` text NOT NULL,
  `email` varchar(225) NOT NULL,
  `no_telephone` varchar(225) NOT NULL,
  `maps` text NOT NULL,
  `address` text NOT NULL,
  `description` text NOT NULL,
  `instagram` varchar(225) NOT NULL,
  `twitter` varchar(225) NOT NULL,
  `facebook` varchar(225) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `tbl_setting`
--

INSERT INTO `tbl_setting` (`id`, `admin_title`, `admin_footer`, `site_title`, `site_footer`, `logo`, `foto_kepsek`, `nama_kepsek`, `text_home`, `email`, `no_telephone`, `maps`, `address`, `description`, `instagram`, `twitter`, `facebook`) VALUES
(1, 'Admin SLB Sunar Aji', 'Copyright © 2022 SLB Sunar Aji.', 'SLB Sunar Aji', '© Hak Cipta Dilindungi 2022 - Website Resmi SLB Sunar Aji, All Rights Reserved', 'b5b850a530a2015975472056bdc67702.png', 'b0aa9cdcd0ab53ec616c547009004346.jpg', 'Agustin Aminah, S.Pd, M.PdI', 'Assalamu\'alaikum Warahmatullahi Wabarakatuh,\r\nSaya ucapkan Selamat Datang di Official Website resmi MTs Negeri 2 Jombang.\r\n\r\nDisertai perasaan bangga saya menuliskan kata sambutan ini, dalam rangka penerbitan Website sekolah edisi perdana, setelah dilakukan update, baik dari sisi pengelolaan maupun isinya.\r\n\r\nDi era global dan pesatnya Teknologi Informasi dan Komunikasi ini, tidak dipungkiri bahwa keberadaan sebuah website untuk suatu organisasi, termasuk MTsN 2 Jombang, sangatlah penting. Wahana website dapat digunakan sebagai media penyebarluasan informasi-informasi dari sekolah, yang memang harus diketahui oleh stake holder secara luas. Disamping itu, website juga dapat menjadi sarana promosi sekolah yang cukup efektif. Sehingga masyarakat dapat mengetahui informasi yang ada di MTsN 2 Jombang.', 'slbsunaraji8278@gmail.com', '081334550766', '<iframe src=\"https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d15817.54570354245!2d112.1991955!3d-7.6415275!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x970cbb1152dac7f6!2sTPA_KB_TK%20Inklusi_SLB%20SUNAR%20AJI!5e0!3m2!1sid!2sid!4v1670845057254!5m2!1sid!2sid\" width=\"100%\" height=\"450\" style=\"border:0;\" allowfullscreen=\"\" loading=\"lazy\" referrerpolicy=\"no-referrer-when-downgrade\"></iframe>', 'Jl. Raya Sukopinggir - Bugasur Kedaleman No.09, Bugasur, Bugasurkedaleman, Kec. Gudo, Kabupaten Jombang, Jawa Timur 61463', 'Website Resmi SLB Sunar Aji Gudo Jombang. Bagi kami kreativitas merupakan gerbang masa depan. kreativitas akan mendorong inovasi. Itulah yang kami lakukan.', 'https://www.instagram.com/slbsunarajijombg/', 'https://twitter.com/', 'https://id-id.facebook.com/');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_siswa`
--

CREATE TABLE `tbl_siswa` (
  `siswa_id` int(11) NOT NULL,
  `siswa_nama` varchar(70) DEFAULT NULL,
  `siswa_jenkel` varchar(2) DEFAULT NULL,
  `siswa_angkatan` varchar(225) NOT NULL,
  `siswa_email` varchar(225) NOT NULL,
  `siswa_photo` varchar(40) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tbl_siswa`
--

INSERT INTO `tbl_siswa` (`siswa_id`, `siswa_nama`, `siswa_jenkel`, `siswa_angkatan`, `siswa_email`, `siswa_photo`) VALUES
(16, 'xikac', 'L', 'xotyma', 'jigoquwup@mailinator.com', NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_slider`
--

CREATE TABLE `tbl_slider` (
  `id` int(11) NOT NULL,
  `image` varchar(225) NOT NULL,
  `caption` varchar(225) NOT NULL,
  `url` varchar(225) NOT NULL,
  `description` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `tbl_slider`
--

INSERT INTO `tbl_slider` (`id`, `image`, `caption`, `url`, `description`) VALUES
(7, '68839b3f942341b3fbbe2e3ce73c70f7.jpg', 'Selamat Datang Di Website SLB Sunar Aji', 'p/sejarah', 'Bagi kami kreativitas merupakan gerbang masa depan. kreativitas akan mendorong inovasi. Itulah yang kami lakukan.'),
(8, 'b03932c3de90cfa0b82791a3c45cf692.jpg', 'Guru Bekualitas Tinggi', '#', 'Guru merupakan faktor penting dalam proses belajar-mengajar. Itulah kenapa kami mendatangkan guru-guru terbaik dari berbagai penjuru.'),
(9, '63f8f94346aaa80afb85463c39233e68.jpg', 'Sekolah Dengan Proses Belajar Interatif', '#', 'Kami membuat proses belajar mengajar menjadi lebih interatif. dengan demikian siswa lebih menyukai proses belajar.');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_submenu`
--

CREATE TABLE `tbl_submenu` (
  `id` int(11) NOT NULL,
  `submenu_name` varchar(225) NOT NULL,
  `submenu_link` varchar(225) NOT NULL,
  `menu_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `tbl_submenu`
--

INSERT INTO `tbl_submenu` (`id`, `submenu_name`, `submenu_link`, `menu_id`) VALUES
(25, 'Visi Dan Misi', 'p/visi-dan-misi', 12),
(26, 'Sejarah', 'p/sejarah', 12),
(27, 'Sambutan Kepala Sekolah', 'p/sambutan-kepala-sekolah', 12),
(29, 'Agenda', 'agenda/', 13),
(30, 'Pengumuman', 'pengumuman/', 13),
(31, 'Artikel', 'artikel/', 13),
(32, 'Galeri Foto', 'galeri/', 18),
(33, 'Galeri Video', 'video/', 18),
(34, 'Pimpinan & Struktur', 'p/pimpinan-struktur', 12),
(35, 'Tenaga Pendidik Kependidikan', 'p/tenaga-pendidik-kependidikan', 12),
(36, 'Vokasi', 'p/vokasi', 12),
(37, 'Program Unggulan', 'p/program-unggulan', 12),
(38, 'P5', 'p/p5', 12),
(39, 'Bina Diri', 'p/bina-diri', 12),
(40, 'Fasilitas', 'p/fasilitas', 12),
(41, 'Video Profile', 'p/video-profile', 12);

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_testimoni`
--

CREATE TABLE `tbl_testimoni` (
  `testimoni_id` int(11) NOT NULL,
  `testimoni_nama` varchar(30) DEFAULT NULL,
  `testimoni_isi` varchar(120) DEFAULT NULL,
  `testimoni_email` varchar(35) DEFAULT NULL,
  `testimoni_tanggal` timestamp NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_tulisan`
--

CREATE TABLE `tbl_tulisan` (
  `tulisan_id` int(11) NOT NULL,
  `tulisan_judul` varchar(100) DEFAULT NULL,
  `tulisan_isi` text DEFAULT NULL,
  `tulisan_deskripsi` text NOT NULL,
  `tulisan_tanggal` timestamp NULL DEFAULT current_timestamp(),
  `tulisan_kategori_id` int(11) DEFAULT NULL,
  `tulisan_kategori_nama` varchar(30) DEFAULT NULL,
  `tulisan_views` int(11) DEFAULT 0,
  `tulisan_gambar` varchar(40) DEFAULT NULL,
  `tulisan_pengguna_id` int(11) DEFAULT NULL,
  `tulisan_author` varchar(40) DEFAULT NULL,
  `tulisan_img_slider` int(2) NOT NULL DEFAULT 0,
  `tulisan_slug` varchar(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_video`
--

CREATE TABLE `tbl_video` (
  `id` int(11) NOT NULL,
  `title` varchar(225) NOT NULL,
  `embed_youtube` varchar(225) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `tbl_video`
--

INSERT INTO `tbl_video` (`id`, `title`, `embed_youtube`) VALUES
(1, 'Sejarah Dan Profil SLB', 'https://www.youtube.com/embed/jW35lZ5jP58');

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `tbl_agenda`
--
ALTER TABLE `tbl_agenda`
  ADD PRIMARY KEY (`agenda_id`);

--
-- Indeks untuk tabel `tbl_album`
--
ALTER TABLE `tbl_album`
  ADD PRIMARY KEY (`album_id`),
  ADD KEY `album_pengguna_id` (`album_pengguna_id`);

--
-- Indeks untuk tabel `tbl_files`
--
ALTER TABLE `tbl_files`
  ADD PRIMARY KEY (`file_id`);

--
-- Indeks untuk tabel `tbl_galeri`
--
ALTER TABLE `tbl_galeri`
  ADD PRIMARY KEY (`galeri_id`),
  ADD KEY `galeri_album_id` (`galeri_album_id`),
  ADD KEY `galeri_pengguna_id` (`galeri_pengguna_id`);

--
-- Indeks untuk tabel `tbl_guru`
--
ALTER TABLE `tbl_guru`
  ADD PRIMARY KEY (`guru_id`);

--
-- Indeks untuk tabel `tbl_inbox`
--
ALTER TABLE `tbl_inbox`
  ADD PRIMARY KEY (`inbox_id`);

--
-- Indeks untuk tabel `tbl_info_ppdb`
--
ALTER TABLE `tbl_info_ppdb`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `tbl_kategori`
--
ALTER TABLE `tbl_kategori`
  ADD PRIMARY KEY (`kategori_id`);

--
-- Indeks untuk tabel `tbl_kelas`
--
ALTER TABLE `tbl_kelas`
  ADD PRIMARY KEY (`kelas_id`);

--
-- Indeks untuk tabel `tbl_komentar`
--
ALTER TABLE `tbl_komentar`
  ADD PRIMARY KEY (`komentar_id`),
  ADD KEY `komentar_tulisan_id` (`komentar_tulisan_id`);

--
-- Indeks untuk tabel `tbl_log_aktivitas`
--
ALTER TABLE `tbl_log_aktivitas`
  ADD PRIMARY KEY (`log_id`),
  ADD KEY `log_pengguna_id` (`log_pengguna_id`);

--
-- Indeks untuk tabel `tbl_menu`
--
ALTER TABLE `tbl_menu`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `tbl_menu_ppdb`
--
ALTER TABLE `tbl_menu_ppdb`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `tbl_page`
--
ALTER TABLE `tbl_page`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `tbl_pengguna`
--
ALTER TABLE `tbl_pengguna`
  ADD PRIMARY KEY (`pengguna_id`);

--
-- Indeks untuk tabel `tbl_pengumuman`
--
ALTER TABLE `tbl_pengumuman`
  ADD PRIMARY KEY (`pengumuman_id`);

--
-- Indeks untuk tabel `tbl_pengunjung`
--
ALTER TABLE `tbl_pengunjung`
  ADD PRIMARY KEY (`pengunjung_id`);

--
-- Indeks untuk tabel `tbl_setting`
--
ALTER TABLE `tbl_setting`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `tbl_siswa`
--
ALTER TABLE `tbl_siswa`
  ADD PRIMARY KEY (`siswa_id`);

--
-- Indeks untuk tabel `tbl_slider`
--
ALTER TABLE `tbl_slider`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `tbl_submenu`
--
ALTER TABLE `tbl_submenu`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `tbl_testimoni`
--
ALTER TABLE `tbl_testimoni`
  ADD PRIMARY KEY (`testimoni_id`);

--
-- Indeks untuk tabel `tbl_tulisan`
--
ALTER TABLE `tbl_tulisan`
  ADD PRIMARY KEY (`tulisan_id`),
  ADD KEY `tulisan_kategori_id` (`tulisan_kategori_id`),
  ADD KEY `tulisan_pengguna_id` (`tulisan_pengguna_id`);

--
-- Indeks untuk tabel `tbl_video`
--
ALTER TABLE `tbl_video`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `tbl_agenda`
--
ALTER TABLE `tbl_agenda`
  MODIFY `agenda_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT untuk tabel `tbl_album`
--
ALTER TABLE `tbl_album`
  MODIFY `album_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT untuk tabel `tbl_files`
--
ALTER TABLE `tbl_files`
  MODIFY `file_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT untuk tabel `tbl_galeri`
--
ALTER TABLE `tbl_galeri`
  MODIFY `galeri_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT untuk tabel `tbl_guru`
--
ALTER TABLE `tbl_guru`
  MODIFY `guru_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT untuk tabel `tbl_inbox`
--
ALTER TABLE `tbl_inbox`
  MODIFY `inbox_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT untuk tabel `tbl_info_ppdb`
--
ALTER TABLE `tbl_info_ppdb`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `tbl_kategori`
--
ALTER TABLE `tbl_kategori`
  MODIFY `kategori_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT untuk tabel `tbl_kelas`
--
ALTER TABLE `tbl_kelas`
  MODIFY `kelas_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT untuk tabel `tbl_komentar`
--
ALTER TABLE `tbl_komentar`
  MODIFY `komentar_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT untuk tabel `tbl_log_aktivitas`
--
ALTER TABLE `tbl_log_aktivitas`
  MODIFY `log_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `tbl_menu`
--
ALTER TABLE `tbl_menu`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT untuk tabel `tbl_menu_ppdb`
--
ALTER TABLE `tbl_menu_ppdb`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT untuk tabel `tbl_page`
--
ALTER TABLE `tbl_page`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=45;

--
-- AUTO_INCREMENT untuk tabel `tbl_pengguna`
--
ALTER TABLE `tbl_pengguna`
  MODIFY `pengguna_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT untuk tabel `tbl_pengumuman`
--
ALTER TABLE `tbl_pengumuman`
  MODIFY `pengumuman_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT untuk tabel `tbl_pengunjung`
--
ALTER TABLE `tbl_pengunjung`
  MODIFY `pengunjung_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `tbl_setting`
--
ALTER TABLE `tbl_setting`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `tbl_siswa`
--
ALTER TABLE `tbl_siswa`
  MODIFY `siswa_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT untuk tabel `tbl_slider`
--
ALTER TABLE `tbl_slider`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT untuk tabel `tbl_submenu`
--
ALTER TABLE `tbl_submenu`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=42;

--
-- AUTO_INCREMENT untuk tabel `tbl_testimoni`
--
ALTER TABLE `tbl_testimoni`
  MODIFY `testimoni_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `tbl_tulisan`
--
ALTER TABLE `tbl_tulisan`
  MODIFY `tulisan_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;

--
-- AUTO_INCREMENT untuk tabel `tbl_video`
--
ALTER TABLE `tbl_video`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
