$(document).ready(function(){
    $(".form-login form").submit(function() {
        var username = $("[name='username']").val();
        var password = $("[name='password']").val();
        //conditions
        if(username.length == 0){
            setTimeout(function() {
                iziToast.error({
                    title: 'ERROR !',
                    message: 'Masukkan username Anda.',
                });
            }, 1000);
        }
        else if(password.length == 0){
            setTimeout(function() {
                iziToast.error({
                    title: 'ERROR !',
                    message: 'Masukkan password Anda.',
                });
            }, 1000);
        }else{  
            return true;
        }
        return false;
    })
});