//run news ticker
$(window).load(function(e) {		
    $("#bn5").breakingNews({
    effect		:"slide-v",
    autoplay	:true,
    timer		:3000,
    });
  });

  $(document).ready(function(){
    $(".search form").submit(function() {
        var q = $("[name='q']").val();
        //conditions
        if(q.length == 0){
            setTimeout(function() {
                iziToast.error({
                    title: 'ERROR !',
                    message: 'Masukkan Kata Kunci dan Enter.',
                });
            }, 1000);
        }else if(q.length  < 3){
            setTimeout(function() {
              iziToast.error({
                  title: 'ERROR !',
                  message: 'Masukkan Kata Kunci Minimal 3 Karakter.',
              });
          }, 1000);          
        }else{  
            return true;
        }
        return false;
    })
});

$(document).ready(function(){
  $(".kontak form").submit(function() {
      var nama  = $("[name='nama']").val();
      var email = $("[name='email']").val();
      var pesan = $("[name='pesan']").val();
      //conditions
      if(nama.length == 0){
          setTimeout(function() {
              iziToast.error({
                  title: 'ERROR !',
                  message: 'Masukkan Nama Lengkap Anda.',
              });
          }, 1000);
      }else if(email.length  == 0){
          setTimeout(function() {
            iziToast.error({
                title: 'ERROR !',
                message: 'Masukkan Alamat Email Anda.',
            });
        }, 1000);          
      }else if(pesan.length == 0) {
        setTimeout(function() {
          iziToast.error({
              title: 'ERROR !',
              message: 'Masukkan Pesan yang Ingin Anda Sampaikan.',
          });
      }, 1000);        
      }else{  
          return true;
      }
      return false;
  })
});